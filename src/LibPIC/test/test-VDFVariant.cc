/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <catch2/catch_all.hpp>

#define LIBPIC_INLINE_VERSION 1
#include <PIC/VDFVariant.h>
#include <algorithm>
#include <cmath>

namespace {
template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Catch::Approx{ b.x }.margin(1e-15)
        && a.y == Catch::Approx{ b.y }.margin(1e-15)
        && a.z == Catch::Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::TensorTemplate<T1, T2> const &a, Detail::TensorTemplate<U1, U2> const &b) noexcept
{
    return a.lo() == b.lo() && a.hi() == b.hi();
}
[[nodiscard]] bool operator==(CurviCoord const &a, CurviCoord const &b) noexcept
{
    return a.q1 == b.q1;
}
} // namespace
using ::operator==;

TEST_CASE("Test LibPIC::VDFVariant::TestParticleVDF", "[LibPIC::VDFVariant::TestParticleVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op;
    Real const theta = 30 * M_PI / 180, D1 = 1.8;
    long const q1min = -7, q1max = 15;
    auto const geo   = Geometry{ theta, D1, O0 };
    auto const Nptls = 2;
    auto const desc  = TestParticleDesc<Nptls>{
        { -O0, op },
        { MFAVector{ 1, 2, 3 }, { 3, 4, 5 } },
        { CurviCoord{ q1min }, CurviCoord{ q1max } }
    };
    auto const vdf = *VDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        CHECK(*vdf.n0(pos) == Catch::Approx{ 0 }.margin(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == Vector{ 0, 0, 0 });
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == Tensor{ 0, 0, 0, 0, 0, 0 });
    }

    // sampling
    auto const n_samples = Nptls + 1;
    auto const particles = vdf.emit(n_samples);

    for (unsigned i = 0; i < Nptls; ++i) {
        auto const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == 0);
        REQUIRE(ptl.psd.real_f == 0);
        REQUIRE(ptl.psd.marker == 1);

        REQUIRE(ptl.pos == desc.pos[i]);
        REQUIRE(geo.cart_to_mfa(ptl.vel, ptl.pos) == desc.vel[i]);

        REQUIRE(vdf.real_f0(ptl) == 0);
    }
    {
        auto const &ptl = particles.back();

        REQUIRE(std::isnan(ptl.vel.x));
        REQUIRE(std::isnan(ptl.vel.y));
        REQUIRE(std::isnan(ptl.vel.z));
        REQUIRE(std::isnan(ptl.pos.q1));
    }
}

TEST_CASE("Test LibPIC::VDFVariant::MaxwellianVDF", "[LibPIC::VDFVariant::MaxwellianVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc{ { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1 };
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = *VDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = MaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = MaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    std::for_each_n(begin(particles), 100, [&](Particle const &ptl) {
        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-15));
    });
}

TEST_CASE("Test LibPIC::VDFVariant::LossconeVDF", "[LibPIC::VDFVariant::LossconeVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, Delta = 0.01, beta = 0.9, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, BiMaxPlasmaDesc{ kinetic, beta1, T2OT1, Vd });
    auto const vdf     = *VDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = LossconeVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = LossconePlasmaDesc({ Delta, beta }, { { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1 / (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = LossconeVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    std::for_each_n(begin(particles), 100, [&](Particle const &ptl) {
        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-15));
    });
}

TEST_CASE("Test LibPIC::VDFVariant::PartialShellVDF", "[LibPIC::VDFVariant::PartialShellVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = .1, vs = 10, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 20;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc{ { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1 };
    auto const desc    = PartialShellPlasmaDesc(kinetic, beta, zeta, vs, Vd);
    auto const vdf     = *VDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = PartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = PartialShellPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = PartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref  = 1;
        auto const nV0_ref = Vector{ Vd, 0, 0 };
        auto const nT_ref  = [xs = desc.vs / std::sqrt(beta)] {
            auto const xs2 = xs * xs;
            auto const Ab  = .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
            return .5 / Ab * (xs * (2.5 + xs2) * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.75 + xs2 * (3 + xs2)) * std::erfc(-xs));
        }() * n0_ref * beta;
        auto const nvv0_ref = Tensor{
            nT_ref / (3 + zeta) + n0_ref * desc.Vd * desc.Vd,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            0,
            0,
            0
        };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    std::for_each_n(begin(particles), 100, [&](Particle const &ptl) {
        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-15));
    });
}
