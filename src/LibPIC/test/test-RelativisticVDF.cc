/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <catch2/catch_all.hpp>

#define LIBPIC_INLINE_VERSION 1
#include <PIC/RelativisticVDF/LossconeVDF.h>
#include <PIC/RelativisticVDF/MaxwellianVDF.h>
#include <PIC/RelativisticVDF/PartialShellVDF.h>
#include <PIC/RelativisticVDF/TestParticleVDF.h>
#include <PIC/UTL/println.h>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <numeric>

namespace {
constexpr bool dump_samples         = false;
constexpr bool enable_moment_checks = false;

using Particle = RelativisticParticle;

[[nodiscard]] bool operator==(Scalar const &a, Scalar const &b) noexcept
{
    return *a == Catch::Approx{ *b }.margin(1e-15);
}
template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Catch::Approx{ b.x }.margin(1e-15)
        && a.y == Catch::Approx{ b.y }.margin(1e-15)
        && a.z == Catch::Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::TensorTemplate<T1, T2> const &a, Detail::TensorTemplate<U1, U2> const &b) noexcept
{
    return a.lo() == b.lo() && a.hi() == b.hi();
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::FourVectorTemplate<T1, T2> const &a, Detail::FourVectorTemplate<U1, U2> const &b) noexcept
{
    return a.t == Catch::Approx{ b.t }.margin(1e-15) && a.s == b.s;
}
template <class T1, class T2, class T3, class U1, class U2, class U3>
[[nodiscard]] bool operator==(Detail::FourTensorTemplate<T1, T2, T3> const &a, Detail::FourTensorTemplate<U1, U2, U3> const &b) noexcept
{
    return a.tt == Catch::Approx{ b.tt }.margin(1e-15)
        && a.ts == b.ts
        && a.ss == b.ss;
}
[[nodiscard]] bool operator==(CurviCoord const &a, CurviCoord const &b) noexcept
{
    return a.q1 == b.q1;
}
} // namespace
using ::operator==;

TEST_CASE("Test LibPIC::RelativisticVDF::TestParticleVDF", "[LibPIC::RelativisticVDF::TestParticleVDF]")
{
    Real const O0 = 1., op = 10 * O0, c = op;
    Real const theta = 30 * M_PI / 180, D1 = 1.8;
    long const q1min = -7, q1max = 15;
    auto const geo   = Geometry{ theta, D1, O0 };
    auto const Nptls = 2;
    auto const desc  = TestParticleDesc<Nptls>{
        { -O0, op },
        { MFAVector{ 1, 2, 3 }, { 3, 4, 5 } },
        { CurviCoord{ q1min }, CurviCoord{ q1max } }
    };
    auto const vdf = RelativisticTestParticleVDF(desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    CHECK(vdf.initial_number_of_test_particles == Nptls);
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        CHECK(*vdf.n0(pos) == Catch::Approx{ 0 }.margin(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == MFAVector{ 0, 0, 0 });
        CHECK(geo.cart_to_mfa(vdf.nuv0(pos), pos) == FourMFATensor{ 0, { 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } });
    }

    // sampling
    auto const n_samples = Nptls + 1;
    auto const particles = vdf.emit(n_samples);

    for (unsigned i = 0; i < Nptls; ++i) {
        auto const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == 0);
        REQUIRE(ptl.psd.real_f == 0);
        REQUIRE(ptl.psd.marker == 1);

        REQUIRE(ptl.pos == desc.pos[i]);
        REQUIRE(geo.cart_to_mfa(ptl.gcgvel, ptl.pos) == lorentz_boost<-1>(FourMFAVector{ c, {} }, desc.vel[i] / c, Real{ ptl.gcgvel.t } / c));

        REQUIRE(vdf.real_f0(ptl) == 0);
        REQUIRE(vdf.g0(ptl) == 1);
    }
    {
        auto const &ptl = particles.back();

        REQUIRE(std::isnan(*ptl.gcgvel.t));
        REQUIRE(std::isnan(ptl.gcgvel.s.x));
        REQUIRE(std::isnan(ptl.gcgvel.s.y));
        REQUIRE(std::isnan(ptl.gcgvel.s.z));
        REQUIRE(std::isnan(ptl.pos.q1));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticTestParticleVDF.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        std::for_each(begin(particles), std::prev(end(particles)), [&os](auto const &ptl) {
            println(os, "    ", ptl, ", ");
        });
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::MaxwellianVDF::NoDrift", "[LibPIC::RelativisticVDF::MaxwellianVDF::NoDrift]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = 2.5, T2OT1 = 5.35;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1);
    auto const vdf     = RelativisticMaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1);
    auto const g_vdf  = RelativisticMaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{};
        auto const nuv0_ref = FourTensor{ 21.69981004243984, {}, { 0.913115349609398, 4.285282263257184, 4.285282263257184, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.margin(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.margin(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.margin(2e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.margin(2e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(2e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const n     = *vdf.n0(ptl.pos);
        auto const vth1  = std::sqrt(beta1);
        auto const vth2  = vth1 * std::sqrt(T2OT1);
        auto const u_mfa = geo.cart_to_mfa(ptl.gcgvel, ptl.pos).s;
        auto const u1    = u_mfa.x;
        auto const u2    = std::sqrt(u_mfa.y * u_mfa.y + u_mfa.z * u_mfa.z);
        auto const f_ref
            = n * std::exp(-u1 * u1 / (vth1 * vth1) - u2 * u2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio) - u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticMaxwellianVDF-no_drift.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::MaxwellianVDF::full_f", "[LibPIC::RelativisticVDF::MaxwellianVDF::full_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = 1.5, T2OT1 = .35, Vd = -3.548;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = RelativisticMaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = RelativisticMaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 37.16749464849181, { -33.2521530093673, 1.77636e-15, 0 }, { 29.81549992828413, 0.1151308643240426, 0.1151308643240426, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * std::sqrt(T2OT1);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1) - u2 * u2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio) - u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticMaxwellianVDF-full_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::MaxwellianVDF::delta_f", "[LibPIC::RelativisticVDF::MaxwellianVDF::delta_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .0001, T2OT1 = 10000, Vd = 1.548;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = RelativisticMaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = RelativisticMaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 1.78788006088261717252e+01, { 6.91911316571424173105e+00, 0, 0 }, { 2.67774157574772431190e+00, 4.35606097460154773060e-01, 4.35606097460154717549e-01, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * std::sqrt(T2OT1);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1) - u2 * u2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio) - u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticMaxwellianVDF-delta_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::LossconeVDF::BiMax", "[LibPIC::RelativisticVDF::LossconeVDF::BiMax]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = 1.5, T2OT1 = .35, Vd = -3.548;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = RelativisticLossconeVDF(LossconePlasmaDesc{ {}, desc }, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = RelativisticLossconeVDF(LossconePlasmaDesc{ {}, g_desc }, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(LossconePlasmaDesc({}, desc)) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 37.16749464849181, { -33.2521530093673, 0, 0 }, { 29.81549992828413, 0.1151308643240426, 0.1151308643240426, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * std::sqrt(T2OT1);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1) - u2 * u2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio) - u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticLossconeVDF-bi_max.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::LossconeVDF::NoDrift", "[LibPIC::RelativisticVDF::LossconeVDF::NoDrift]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = 1.543, vth2Ovth1 = 1.35, Vd = 0;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0, Delta = .155, beta = .132;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, kinetic, beta1, vth2Ovth1 * vth2Ovth1, Vd);
    auto const vdf     = RelativisticLossconeVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, std::pow(vth2Ovth1, 2) * (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = RelativisticLossconeVDF(LossconePlasmaDesc{ { Delta, beta }, g_desc }, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 17.79534683716217, { 0, 0, 0 }, { 0.672092580685977, 1.333510789085709, 1.333510789085709, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.margin(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.margin(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.margin(2e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.margin(2e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(2e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * vth2Ovth1;
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1)) / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2) / (1 - beta)
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * beta)));
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio)) / (1 - beta)
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio))
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio * beta)));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticLossconeVDF-no_drift.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::LossconeVDF::full_f", "[LibPIC::RelativisticVDF::LossconeVDF::full_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = 2.543, vth2Ovth1 = .35, Vd = -2.594;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0, Delta = .155, beta = 3.132;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, kinetic, beta1, vth2Ovth1 * vth2Ovth1, Vd);
    auto const vdf     = RelativisticLossconeVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, std::pow(vth2Ovth1, 2) * (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = RelativisticLossconeVDF(LossconePlasmaDesc{ { Delta, beta }, g_desc }, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 23.13160472988927, { -15.55382477304072, 0, 0 }, { 10.93936030662703, 0.3953503978366635, 0.3953503978366635, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(2e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * vth2Ovth1;
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1)) / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2) / (1 - beta)
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * beta)));
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio)) / (1 - beta)
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio))
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio * beta)));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticLossconeVDF-full_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::LossconeVDF::delta_f", "[LibPIC::RelativisticVDF::LossconeVDF::delta_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .01, vth2Ovth1 = 30.35, Vd = -1.594;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0, Delta = 0, beta = .3845;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, kinetic, beta1, vth2Ovth1 * vth2Ovth1, Vd);
    auto const vdf     = RelativisticLossconeVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, std::pow(vth2Ovth1, 2) * (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = RelativisticLossconeVDF(LossconePlasmaDesc{ { Delta, beta }, g_desc }, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 2.30940677791666004737e+01, { -9.20439814816789514396e+00, 4.44089209850062616169e-16, 0 }, { 3.67149629609761163351e+00, 4.04034569551620492689e+00, 4.04034569551620492689e+00, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.margin(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(2e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta1);
        auto const vth2 = vth1 * vth2Ovth1;
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1)) / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2) / (1 - beta)
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * beta)));
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio)) / (1 - beta)
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio))
            * ((1 - Delta * beta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio)) - (1 - Delta) * std::exp(-u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio * beta)));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticLossconeVDF-delta_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::PartialShellVDF::Maxwellian", "[LibPIC::RelativisticVDF::PartialShellVDF::Maxwellian]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = 1.5, vs = 0, Vd = 2.5943;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 0;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = PartialShellPlasmaDesc(kinetic, beta, zeta, vs, Vd);
    auto const vdf     = RelativisticPartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc(kinetic, beta, 1, Vd);
    auto const g_vdf  = RelativisticMaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        REQUIRE(vdf.n0(pos) == g_vdf.n0(pos));
        REQUIRE(vdf.nV0(pos) == g_vdf.nV0(pos));
        REQUIRE(vdf.nuv0(pos) == g_vdf.nuv0(pos));
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.margin(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.margin(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ g_vdf.f0(ptl) / g_vdf.g0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.g0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth1 = std::sqrt(beta);
        auto const vth2 = vth1 * std::sqrt(g_desc.T2_T1);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u1   = u_co.x;
        auto const u2   = std::sqrt(u_co.y * u_co.y + u_co.z * u_co.z);
        auto const f_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1) - u2 * u2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n0 * std::exp(-u1 * u1 / (vth1 * vth1 * desc.marker_temp_ratio) - u2 * u2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticPartialShellVDF-maxwellian.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::PartialShellVDF::IsoShell", "[LibPIC::RelativisticVDF::PartialShellVDF::IsoShell]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = .5, vs = 10, Vd = -2.8476;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 0;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = PartialShellPlasmaDesc(kinetic, beta, zeta, vs, Vd);
    auto const vdf     = RelativisticPartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = PartialShellPlasmaDesc(kinetic, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = RelativisticPartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 70.60133254746985, { -56.48486468351399, 0, 0 }, { 48.95406153422953, 8.7424845428378, 8.7424845428378, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.margin(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.margin(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(1e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(3e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(2e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth  = std::sqrt(beta);
        auto const b    = vs / vth;
        auto const Ab   = .5 * (b * std::exp(-b * b) + 2 / M_2_SQRTPI * (.5 + b * b) * std::erfc(-b));
        auto const Bz   = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u    = std::sqrt(dot(u_co, u_co));
        auto const cosa = u_co.x / u;
        auto const f_ref
            = n0 * std::exp(-std::pow(u - vs, 2) / (vth * vth)) * std::pow((1 - cosa) * (1 + cosa), .5 * zeta)
            / (2 * M_PI * Ab * Bz * vth * vth * vth);

        auto const marker_b  = vs / (vth * std::sqrt(desc.marker_temp_ratio));
        auto const marker_Ab = .5 * (marker_b * std::exp(-marker_b * marker_b) + 2 / M_2_SQRTPI * (.5 + marker_b * marker_b) * std::erfc(-marker_b));
        auto const g_ref
            = n0 * std::exp(-std::pow(u - vs, 2) / (vth * vth * desc.marker_temp_ratio)) * std::pow((1 - cosa) * (1 + cosa), .5 * zeta)
            / (2 * M_PI * marker_Ab * Bz * vth * vth * vth * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticPartialShellVDF-iso_shell.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::RelativisticVDF::PartialShellVDF::AniShell", "[LibPIC::RelativisticVDF::PartialShellVDF::AniShell]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = 1.5, vs = 10, Vd = 2.8476;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 30;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = PartialShellPlasmaDesc(kinetic, beta, zeta, vs, Vd);
    auto const vdf     = RelativisticPartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = PartialShellPlasmaDesc(kinetic, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = RelativisticPartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 62.98502804152568, { 45.41106563176449, 3.55271e-15, 0 }, { 33.13165520859149, 12.85628134020963, 12.85628134020963, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [c, n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.velocity(c) * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.margin(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.margin(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), FourCartTensor{}, [c, n_samples, is_delta_f = desc.scheme](FourCartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.velocity(c);
                auto const u = ptl.gcgvel.s;
                return sum + FourCartTensor{ c * ptl.gcgvel.t, c * ptl.gcgvel.s, { v.x * u.x, v.y * u.y, v.z * u.z, v.x * u.y, v.y * u.z, v.z * u.x } } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(*stress_energy.tt == Catch::Approx{ *vdf.nuv0(CurviCoord{}).tt }.epsilon(1e-2));
        CHECK(stress_energy.ts.x == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.x }.epsilon(1e-2));
        CHECK(stress_energy.ts.y == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.y }.epsilon(1e-2));
        CHECK(stress_energy.ts.z == Catch::Approx{ vdf.nuv0(CurviCoord{}).ts.z }.margin(2e-2));
        CHECK(stress_energy.ss.xx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xx }.epsilon(1e-2));
        CHECK(stress_energy.ss.yy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yy }.epsilon(1e-2));
        CHECK(stress_energy.ss.zz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zz }.epsilon(1e-2));
        CHECK(stress_energy.ss.xy == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.xy }.epsilon(1e-2));
        CHECK(stress_energy.ss.yz == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.yz }.margin(1e-2));
        CHECK(stress_energy.ss.zx == Catch::Approx{ vdf.nuv0(CurviCoord{}).ss.zx }.margin(2e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-13));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));

        auto const gd   = c / std::sqrt((c - Vd) * (c + Vd));
        auto const n0   = *vdf.n0(ptl.pos) / gd;
        auto const vth  = std::sqrt(beta);
        auto const b    = vs / vth;
        auto const Ab   = .5 * (b * std::exp(-b * b) + 2 / M_2_SQRTPI * (.5 + b * b) * std::erfc(-b));
        auto const Bz   = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);
        auto const u_co = lorentz_boost<+1>(geo.cart_to_mfa(ptl.gcgvel, ptl.pos), Vd / c, gd).s;
        auto const u    = std::sqrt(dot(u_co, u_co));
        auto const cosa = u_co.x / u;
        auto const f_ref
            = n0 * std::exp(-std::pow(u - vs, 2) / (vth * vth)) * std::pow((1 - cosa) * (1 + cosa), .5 * zeta)
            / (2 * M_PI * Ab * Bz * vth * vth * vth);

        auto const marker_b  = vs / (vth * std::sqrt(desc.marker_temp_ratio));
        auto const marker_Ab = .5 * (marker_b * std::exp(-marker_b * marker_b) + 2 / M_2_SQRTPI * (.5 + marker_b * marker_b) * std::erfc(-marker_b));
        auto const g_ref
            = n0 * std::exp(-std::pow(u - vs, 2) / (vth * vth * desc.marker_temp_ratio)) * std::pow((1 - cosa) * (1 + cosa), .5 * zeta)
            / (2 * M_PI * marker_Ab * Bz * vth * vth * vth * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/RelativisticPartialShellVDF-ani_shell.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}
