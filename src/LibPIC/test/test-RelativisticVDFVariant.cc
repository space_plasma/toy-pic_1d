/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <catch2/catch_all.hpp>

#define LIBPIC_INLINE_VERSION 1
#include <PIC/RelativisticVDFVariant.h>
#include <algorithm>
#include <cmath>

namespace {
using Particle = RelativisticParticle;

[[nodiscard]] bool operator==(Scalar const &a, Scalar const &b) noexcept
{
    return *a == Catch::Approx{ *b }.margin(1e-15);
}
template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Catch::Approx{ b.x }.margin(1e-15)
        && a.y == Catch::Approx{ b.y }.margin(1e-15)
        && a.z == Catch::Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::TensorTemplate<T1, T2> const &a, Detail::TensorTemplate<U1, U2> const &b) noexcept
{
    return a.lo() == b.lo() && a.hi() == b.hi();
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::FourVectorTemplate<T1, T2> const &a, Detail::FourVectorTemplate<U1, U2> const &b) noexcept
{
    return a.t == Catch::Approx{ b.t }.margin(1e-15) && a.s == b.s;
}
template <class T1, class T2, class T3, class U1, class U2, class U3>
[[nodiscard]] bool operator==(Detail::FourTensorTemplate<T1, T2, T3> const &a, Detail::FourTensorTemplate<U1, U2, U3> const &b) noexcept
{
    return a.tt == Catch::Approx{ b.tt }.margin(1e-15)
        && a.ts == b.ts
        && a.ss == b.ss;
}
[[nodiscard]] bool operator==(CurviCoord const &a, CurviCoord const &b) noexcept
{
    return a.q1 == b.q1;
}
} // namespace
using ::operator==;

TEST_CASE("Test LibPIC::RelativisticVDFVariant::TestParticleVDF", "[LibPIC::RelativisticVDFVariant::TestParticleVDF]")
{
    Real const O0 = 1., op = 10 * O0, c = op;
    Real const theta = 30 * M_PI / 180, D1 = 1.8;
    long const q1min = -7, q1max = 15;
    auto const geo   = Geometry{ theta, D1, O0 };
    auto const Nptls = 2;
    auto const desc  = TestParticleDesc<Nptls>{
        { -O0, op },
        { MFAVector{ 1, 2, 3 }, { 3, 4, 5 } },
        { CurviCoord{ q1min }, CurviCoord{ q1max } }
    };
    auto const vdf = *RelativisticVDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        CHECK(vdf.n0(pos) == Scalar{ 0 });
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == Vector{ 0, 0, 0 });
        CHECK(geo.cart_to_mfa(vdf.nuv0(pos), pos) == FourTensor{ 0, { 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } });
    }

    // sampling
    auto const n_samples = Nptls + 1;
    auto const particles = vdf.emit(n_samples);

    for (unsigned i = 0; i < Nptls; ++i) {
        auto const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == 0);
        REQUIRE(ptl.psd.real_f == 0);
        REQUIRE(ptl.psd.marker == 1);

        REQUIRE(ptl.pos == desc.pos[i]);
        REQUIRE(geo.cart_to_mfa(ptl.gcgvel, ptl.pos) == lorentz_boost<-1>(FourMFAVector{ c, {} }, desc.vel[i] / c, Real{ ptl.gcgvel.t } / c));

        REQUIRE(vdf.real_f0(ptl) == 0);
    }
    {
        auto const &ptl = particles.back();

        REQUIRE(std::isnan(*ptl.gcgvel.t));
        REQUIRE(std::isnan(ptl.gcgvel.s.x));
        REQUIRE(std::isnan(ptl.gcgvel.s.y));
        REQUIRE(std::isnan(ptl.gcgvel.s.z));
        REQUIRE(std::isnan(ptl.pos.q1));
    }
}

TEST_CASE("Test LibPIC::RelativisticVDFVariant::MaxwellianVDF", "[LibPIC::RelativisticVDFVariant::MaxwellianVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .0001, T2OT1 = 10000, Vd = 1.548;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = *RelativisticVDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = RelativisticMaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = RelativisticMaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 1.78788006088261717252e+01, { 6.91911316571424173105e+00, 0, 0 }, { 2.67774157574772431190e+00, 4.35606097460154773060e-01, 4.35606097460154717549e-01, 0, 0, 0 } };

        REQUIRE(vdf.n0(pos) == Scalar{ n0_ref });
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));
    }
}

TEST_CASE("Test LibPIC::RelativisticVDFVariant::LossconeVDF", "[LibPIC::RelativisticVDFVariant::LossconeVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .01, vth2Ovth1 = 30.35, Vd = -1.594;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0, Delta = 0, beta = .3845;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, kinetic, beta1, vth2Ovth1 * vth2Ovth1, Vd);
    auto const vdf     = *RelativisticVDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = RelativisticLossconeVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, std::pow(vth2Ovth1, 2) * (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = RelativisticLossconeVDF(LossconePlasmaDesc{ { Delta, beta }, g_desc }, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 2.30940677791666004737e+01, { -9.20439814816789514396e+00, 4.44089209850062616169e-16, 0 }, { 3.67149629609761163351e+00, 4.04034569551620492689e+00, 4.04034569551620492689e+00, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * f_vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));
    }
}

TEST_CASE("Test LibPIC::RelativisticVDFVariant::PartialShellVDF", "[LibPIC::RelativisticVDFVariant::PartialShellVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = 1.5, vs = 10, Vd = 2.8476;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 30;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, 0, 2.1);
    auto const desc    = PartialShellPlasmaDesc(kinetic, beta, zeta, vs, Vd);
    auto const vdf     = *RelativisticVDFVariant::make(desc, geo, Range{ q1min, q1max - q1min }, c);

    auto const f_vdf  = RelativisticPartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);
    auto const g_desc = PartialShellPlasmaDesc(kinetic, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = RelativisticPartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(kinetic) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = n0_ref * Vector{ Vd, 0, 0 };
        auto const nuv0_ref = FourTensor{ 62.98502804152568, { 45.41106563176449, 3.55271e-15, 0 }, { 33.13165520859149, 12.85628134020963, 12.85628134020963, 0, 0, 0 } };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nuv0(pos), pos) == nuv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight * desc.scheme + (1 - desc.scheme) * f_vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-13));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ f_vdf.f0(ptl) * desc.scheme + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ f_vdf.f0(ptl) }.epsilon(1e-14));
        REQUIRE(ptl.gcgvel.t == Catch::Approx{ std::sqrt(c * c + dot(ptl.gcgvel.s, ptl.gcgvel.s)) }.epsilon(1e-10));
    }
}
