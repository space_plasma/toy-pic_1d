/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <catch2/catch_all.hpp>

#define LIBPIC_INLINE_VERSION 1
#include <PIC/UTL/println.h>
#include <PIC/VDF/LossconeVDF.h>
#include <PIC/VDF/MaxwellianVDF.h>
#include <PIC/VDF/PartialShellVDF.h>
#include <PIC/VDF/TestParticleVDF.h>
#include <algorithm>
#include <cmath>
#include <fstream>

namespace {
constexpr bool dump_samples         = false;
constexpr bool enable_moment_checks = false;

template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Catch::Approx{ b.x }.margin(1e-15)
        && a.y == Catch::Approx{ b.y }.margin(1e-15)
        && a.z == Catch::Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::TensorTemplate<T1, T2> const &a, Detail::TensorTemplate<U1, U2> const &b) noexcept
{
    return a.lo() == b.lo() && a.hi() == b.hi();
}
[[nodiscard]] bool operator==(CurviCoord const &a, CurviCoord const &b) noexcept
{
    return a.q1 == b.q1;
}
} // namespace
using ::operator==;

TEST_CASE("Test LibPIC::VDF::TestParticleVDF", "[LibPIC::VDF::TestParticleVDF]")
{
    Real const O0 = 1., op = 4 * O0, c = op;
    Real const theta = 30 * M_PI / 180, D1 = 1.8;
    long const q1min = -7, q1max = 15;
    auto const geo   = Geometry{ theta, D1, O0 };
    auto const Nptls = 2;
    auto const desc  = TestParticleDesc<Nptls>{
        { -O0, op },
        { MFAVector{ 1, 2, 3 }, { 3, 4, 5 } },
        { CurviCoord{ q1min }, CurviCoord{ q1max } }
    };
    auto const vdf = TestParticleVDF(desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    CHECK(vdf.initial_number_of_test_particles == Nptls);
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        CHECK(*vdf.n0(pos) == Catch::Approx{ 0 }.margin(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == Vector{ 0, 0, 0 });
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == Tensor{ 0, 0, 0, 0, 0, 0 });
    }

    // sampling
    auto const n_samples = Nptls + 1;
    auto const particles = vdf.emit(n_samples);

    for (unsigned i = 0; i < Nptls; ++i) {
        auto const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == 0);
        REQUIRE(ptl.psd.real_f == 0);
        REQUIRE(ptl.psd.marker == 1);

        REQUIRE(ptl.pos == desc.pos[i]);
        REQUIRE(geo.cart_to_mfa(ptl.vel, ptl.pos) == desc.vel[i]);

        REQUIRE(vdf.real_f0(ptl) == 0);
        REQUIRE(vdf.g0(ptl) == 1);
    }
    {
        auto const &ptl = particles.back();

        REQUIRE(std::isnan(ptl.vel.x));
        REQUIRE(std::isnan(ptl.vel.y));
        REQUIRE(std::isnan(ptl.vel.z));
        REQUIRE(std::isnan(ptl.pos.q1));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/TestParticleVDF.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        std::for_each(begin(particles), std::prev(end(particles)), [&os](auto const &ptl) {
            println(os, "    ", ptl, ", ");
        });
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::MaxwellianVDF::full_f", "[LibPIC::VDF::MaxwellianVDF::full_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = 1.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87;
    long const q1min = -7, q1max = 15;
    auto const geo  = Geometry{ theta, D1, O0 };
    auto const desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1, T2OT1, Vd);
    auto const vdf  = MaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    std::for_each_n(begin(particles), 100, [&](Particle const &ptl) {
        REQUIRE(ptl.psd.weight == 1);
        REQUIRE(ptl.psd.marker == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n     = *vdf.n0(ptl.pos);
        auto const vth1  = std::sqrt(beta1);
        auto const vth2  = vth1 * std::sqrt(T2OT1);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1) - v2 * v2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1 * desc.marker_temp_ratio) - v2 * v2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    });

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/MaxwellianVDF-full_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::MaxwellianVDF::delta_f", "[LibPIC::VDF::MaxwellianVDF::delta_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc{ { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1 };
    auto const desc    = BiMaxPlasmaDesc(kinetic, beta1, T2OT1, Vd);
    auto const vdf     = MaxwellianVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = MaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    std::for_each_n(begin(particles), 100, [&](Particle const &ptl) {
        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n     = *vdf.n0(ptl.pos);
        auto const vth1  = std::sqrt(beta1);
        auto const vth2  = vth1 * std::sqrt(T2OT1);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1) - v2 * v2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1 * desc.marker_temp_ratio) - v2 * v2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    });

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/MaxwellianVDF-delta_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::LossconeVDF::BiMax", "[LibPIC::VDF::LossconeVDF::BiMax]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = 1.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo  = Geometry{ theta, D1, O0 };
    auto const desc = [&] {
        KineticPlasmaDesc const desc = { { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, .001, 2.1 };
        return BiMaxPlasmaDesc{ desc, beta1, T2OT1, Vd };
    }();
    auto const vdf = LossconeVDF(LossconePlasmaDesc({}, desc), geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = MaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c); // intentionally MaxwellianVDF

    CHECK(serialize(LossconePlasmaDesc({}, desc)) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.scheme == ParticleScheme::delta_f ? desc.initial_weight : vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * long(desc.scheme == ParticleScheme::delta_f) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n     = *vdf.n0(ptl.pos);
        auto const vth1  = std::sqrt(beta1);
        auto const vth2  = vth1 * std::sqrt(T2OT1);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1) - v2 * v2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1 * desc.marker_temp_ratio) - v2 * v2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/LossconeVDF-bi_max.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::LossconeVDF::full_f", "[LibPIC::VDF::LossconeVDF::full_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = 1.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, Delta = 0.01, beta = 0.9, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, 0, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, kinetic, beta1, T2OT1 / (1 + (1 - Delta) * beta), Vd);
    auto const vdf     = LossconeVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1, Vd);
    auto const g_vdf  = LossconeVDF(LossconePlasmaDesc({ Delta, beta }, g_desc), geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n                 = *vdf.n0(ptl.pos);
        auto const vth_ratio_squared = T2OT1 / (1 + (1 - Delta) * beta);
        auto const vth1              = std::sqrt(beta1);
        auto const vth2              = vth1 * std::sqrt(vth_ratio_squared);
        auto const v_mfa             = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1                = v_mfa.x - Vd;
        auto const v2                = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1))
            * ((1 - Delta * beta) * std::exp(-v2 * v2 / (vth2 * vth2)) - (1 - Delta) * std::exp(-v2 * v2 / (beta * vth2 * vth2)))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2) / (1 - beta);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (desc.marker_temp_ratio * vth1 * vth1))
            * ((1 - Delta * beta) * std::exp(-v2 * v2 / (desc.marker_temp_ratio * vth2 * vth2)) - (1 - Delta) * std::exp(-v2 * v2 / (beta * desc.marker_temp_ratio * vth2 * vth2)))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio)) / (1 - beta);

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/LossconeVDF-full_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::LossconeVDF::delta_f", "[LibPIC::VDF::LossconeVDF::delta_f]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta1 = .1, T2OT1 = 5.35, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, Delta = 0.01, beta = 0.9, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15;
    auto const geo     = Geometry{ theta, D1, O0 };
    auto const kinetic = KineticPlasmaDesc({ -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1);
    auto const desc    = LossconePlasmaDesc({ Delta, beta }, BiMaxPlasmaDesc{ kinetic, beta1, T2OT1, Vd });
    auto const vdf     = LossconeVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = LossconePlasmaDesc({ Delta, beta }, { { -O0, op }, 10, ShapeOrder::CIC }, beta1 * desc.marker_temp_ratio, T2OT1 / (1 + (1 - Delta) * beta), Vd);
    auto const g_vdf  = LossconeVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta1 / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta1 / 2 * desc.T2_T1,
            desc.beta1 / 2 * desc.T2_T1,
            0,
            0,
            0
        };

        CHECK(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        CHECK(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        CHECK(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(2e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n                 = *vdf.n0(ptl.pos);
        auto const vth_ratio_squared = T2OT1 / (1 + (1 - Delta) * beta);
        auto const vth1              = std::sqrt(beta1);
        auto const vth2              = vth1 * std::sqrt(vth_ratio_squared);
        auto const v_mfa             = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1                = v_mfa.x - Vd;
        auto const v2                = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1))
            * ((1 - Delta * beta) * std::exp(-v2 * v2 / (vth2 * vth2)) - (1 - Delta) * std::exp(-v2 * v2 / (beta * vth2 * vth2)))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2) / (1 - beta);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (desc.marker_temp_ratio * vth1 * vth1))
            * ((1 - Delta * beta) * std::exp(-v2 * v2 / (desc.marker_temp_ratio * vth2 * vth2)) - (1 - Delta) * std::exp(-v2 * v2 / (beta * desc.marker_temp_ratio * vth2 * vth2)))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio)) / (1 - beta);

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/LossconeVDF-delta_f.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::PartialShellVDF::BiMax", "[LibPIC::VDF::PartialShellVDF::BiMax]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = .1, vs = 0, Vd = 1.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 0;
    auto const geo  = Geometry{ theta, D1, O0 };
    auto const desc = [&] {
        KineticPlasmaDesc const desc = { { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, .001, 2.1 };
        return PartialShellPlasmaDesc(desc, beta, zeta, vs, Vd);
    }();
    auto const vdf = PartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = BiMaxPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta * desc.marker_temp_ratio, 1, Vd);
    auto const g_vdf  = MaxwellianVDF(g_desc, geo, { q1min, q1max - q1min }, c); // intentionally MaxwellianVDF

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref   = 1;
        auto const nV0_ref  = Vector{ Vd, 0, 0 };
        auto const nvv0_ref = Tensor{
            desc.beta / 2 + n0_ref * desc.Vd * desc.Vd,
            desc.beta / 2 * g_desc.T2_T1,
            desc.beta / 2 * g_desc.T2_T1,
            0,
            0,
            0
        };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.scheme == ParticleScheme::delta_f ? desc.initial_weight : vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-13));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) * long(desc.scheme == ParticleScheme::delta_f) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n     = *vdf.n0(ptl.pos);
        auto const vth1  = std::sqrt(beta);
        auto const vth2  = vth1 * std::sqrt(g_desc.T2_T1);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const f_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1) - v2 * v2 / (vth2 * vth2))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2);
        auto const g_ref
            = n * std::exp(-v1 * v1 / (vth1 * vth1 * desc.marker_temp_ratio) - v2 * v2 / (vth2 * vth2 * desc.marker_temp_ratio))
            / (4 * M_PI_2 / M_2_SQRTPI * vth1 * vth2 * vth2 * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/PartialShellVDF-bi_max.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::PartialShellVDF::IsoShell", "[LibPIC::VDF::PartialShellVDF::IsoShell]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = .1, vs = 10, Vd = 1.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 0;
    auto const geo  = Geometry{ theta, D1, O0 };
    auto const desc = [&] {
        KineticPlasmaDesc const desc = { { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::full_f, .001, 2.1 };
        return PartialShellPlasmaDesc(desc, beta, zeta, vs, Vd);
    }();
    auto const vdf = PartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = PartialShellPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = PartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref  = 1;
        auto const nV0_ref = Vector{ Vd, 0, 0 };
        auto const nT_ref  = [xs = desc.vs / std::sqrt(beta)] {
            auto const xs2 = xs * xs;
            auto const Ab  = .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
            return .5 / Ab * (xs * (2.5 + xs2) * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.75 + xs2 * (3 + xs2)) * std::erfc(-xs));
        }() * n0_ref * beta;
        auto const nvv0_ref = Tensor{
            nT_ref / (3 + zeta) + n0_ref * desc.Vd * desc.Vd,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            0,
            0,
            0
        };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(5e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(1e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(1e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ vdf.f0(ptl) / g_vdf.f0(ptl) }.margin(1e-15));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ ptl.psd.weight * ptl.psd.marker }.epsilon(1e-10));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n   = *vdf.n0(ptl.pos);
        auto const vth = std::sqrt(beta);
        auto const Ab  = [xs = desc.vs / vth] {
            auto const xs2 = xs * xs;
            return .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
        }();
        auto const marker_Ab = [xs = desc.vs / vth / std::sqrt(desc.marker_temp_ratio)] {
            auto const xs2 = xs * xs;
            return .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
        }();
        auto const Bz    = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const v     = std::sqrt(v1 * v1 + v2 * v2);
        auto const f_ref
            = n * std::exp(-(v - vs) * (v - vs) / (vth * vth)) * std::pow(v2 / v, zeta)
            / (2 * M_PI * vth * vth * vth * Ab * Bz);
        auto const g_ref
            = n * std::exp(-(v - vs) * (v - vs) / (vth * vth * desc.marker_temp_ratio)) * std::pow(v2 / v, zeta)
            / (2 * M_PI * vth * vth * vth * marker_Ab * Bz * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/PartialShellVDF-iso_shell.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}

TEST_CASE("Test LibPIC::VDF::PartialShellVDF::AniShell", "[LibPIC::VDF::PartialShellVDF::AniShell]")
{
    Real const O0 = 1., op = 4 * O0, c = op, beta = .1, vs = 10, Vd = -10.549;
    Real const theta = 30 * M_PI / 180, D1 = 1.87, psd_refresh_frequency = 0;
    long const q1min = -7, q1max = 15, zeta = 20;
    auto const geo  = Geometry{ theta, D1, O0 };
    auto const desc = [&] {
        KineticPlasmaDesc const desc = { { -O0, op }, 10, ShapeOrder::CIC, psd_refresh_frequency, ParticleScheme::delta_f, .001, 2.1 };
        return PartialShellPlasmaDesc(desc, beta, zeta, vs, Vd);
    }();
    auto const vdf = PartialShellVDF(desc, geo, { q1min, q1max - q1min }, c);

    auto const g_desc = PartialShellPlasmaDesc({ { -O0, op }, 10, ShapeOrder::CIC }, beta * desc.marker_temp_ratio, zeta, vs, Vd);
    auto const g_vdf  = PartialShellVDF(g_desc, geo, { q1min, q1max - q1min }, c);

    CHECK(serialize(desc) == serialize(vdf.plasma_desc()));

    // check equilibrium macro variables
    CHECK(vdf.Nrefcell_div_Ntotal() == Catch::Approx{ 1.0 / (q1max - q1min) }.epsilon(1e-10));

    for (long q1 = q1min; q1 <= q1max; ++q1) {
        CurviCoord const pos(q1);

        auto const n0_ref  = 1;
        auto const nV0_ref = Vector{ Vd, 0, 0 };
        auto const nT_ref  = [xs = desc.vs / std::sqrt(beta)] {
            auto const xs2 = xs * xs;
            auto const Ab  = .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
            return .5 / Ab * (xs * (2.5 + xs2) * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.75 + xs2 * (3 + xs2)) * std::erfc(-xs));
        }() * n0_ref * beta;
        auto const nvv0_ref = Tensor{
            nT_ref / (3 + zeta) + n0_ref * desc.Vd * desc.Vd,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            nT_ref / (3 + zeta) * (2 + zeta) * .5,
            0,
            0,
            0
        };

        REQUIRE(*vdf.n0(pos) == Catch::Approx{ n0_ref }.epsilon(1e-10));
        REQUIRE(geo.cart_to_mfa(vdf.nV0(pos), pos) == nV0_ref);
        REQUIRE(geo.cart_to_mfa(vdf.nvv0(pos), pos) == nvv0_ref);
    }

    // sampling
    auto const n_samples = 100000U;
    auto const particles = vdf.emit(n_samples);

    // moments
    if constexpr (enable_moment_checks) {
        auto const particle_density = std::accumulate(
            begin(particles), end(particles), Real{}, [is_delta_f = desc.scheme](Real const sum, Particle const &ptl) {
                return sum + 1 * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_density == Catch::Approx{ vdf.n0(CurviCoord{}) }.epsilon(1e-2));

        auto const particle_flux = std::accumulate(
            begin(particles), end(particles), CartVector{}, [n_samples, is_delta_f = desc.scheme](CartVector const &sum, Particle const &ptl) {
                return sum + ptl.vel * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(particle_flux.x == Catch::Approx{ vdf.nV0(CurviCoord{}).x }.epsilon(1e-2));
        CHECK(particle_flux.y == Catch::Approx{ vdf.nV0(CurviCoord{}).y }.epsilon(1e-2));
        CHECK(particle_flux.z == Catch::Approx{ vdf.nV0(CurviCoord{}).z }.margin(1e-2));

        auto const stress_energy = std::accumulate(
            begin(particles), end(particles), CartTensor{}, [n_samples, is_delta_f = desc.scheme](CartTensor const &sum, Particle const &ptl) {
                auto const v = ptl.vel;
                return sum + CartTensor{ v.x * v.x, v.y * v.y, v.z * v.z, v.x * v.y, v.y * v.z, v.z * v.x } * (ptl.psd.real_f / ptl.psd.marker + is_delta_f * ptl.psd.weight) / n_samples;
            });
        CHECK(stress_energy.xx == Catch::Approx{ vdf.nvv0(CurviCoord{}).xx }.epsilon(1e-2));
        CHECK(stress_energy.yy == Catch::Approx{ vdf.nvv0(CurviCoord{}).yy }.epsilon(1e-2));
        CHECK(stress_energy.zz == Catch::Approx{ vdf.nvv0(CurviCoord{}).zz }.epsilon(1e-2));
        CHECK(stress_energy.xy == Catch::Approx{ vdf.nvv0(CurviCoord{}).xy }.epsilon(1e-2));
        CHECK(stress_energy.yz == Catch::Approx{ vdf.nvv0(CurviCoord{}).yz }.margin(2e-2));
        CHECK(stress_energy.zx == Catch::Approx{ vdf.nvv0(CurviCoord{}).zx }.margin(4e-2));
    }

    static_assert(n_samples > 100);
    for (unsigned long i = 0; i < 100; ++i) {
        Particle const &ptl = particles[i];

        REQUIRE(ptl.psd.weight == Catch::Approx{ desc.initial_weight }.margin(1e-10));
        REQUIRE(ptl.psd.marker == Catch::Approx{ g_vdf.f0(ptl) }.epsilon(1e-15));
        REQUIRE(ptl.psd.real_f == Catch::Approx{ vdf.f0(ptl) + ptl.psd.weight * ptl.psd.marker }.epsilon(1e-15));
        REQUIRE(vdf.real_f0(ptl) == Catch::Approx{ vdf.f0(ptl) }.epsilon(1e-15));

        auto const n   = *vdf.n0(ptl.pos);
        auto const vth = std::sqrt(beta);
        auto const Ab  = [xs = desc.vs / vth] {
            auto const xs2 = xs * xs;
            return .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
        }();
        auto const marker_Ab = [xs = desc.vs / vth / std::sqrt(desc.marker_temp_ratio)] {
            auto const xs2 = xs * xs;
            return .5 * (xs * std::exp(-xs2) + 2 / M_2_SQRTPI * (0.5 + xs2) * std::erfc(-xs));
        }();
        auto const Bz    = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);
        auto const v_mfa = geo.cart_to_mfa(ptl.vel, ptl.pos);
        auto const v1    = v_mfa.x - Vd;
        auto const v2    = std::sqrt(v_mfa.y * v_mfa.y + v_mfa.z * v_mfa.z);
        auto const v     = std::sqrt(v1 * v1 + v2 * v2);
        auto const f_ref
            = n * std::exp(-(v - vs) * (v - vs) / (vth * vth)) * std::pow(v2 / v, zeta)
            / (2 * M_PI * vth * vth * vth * Ab * Bz);
        auto const g_ref
            = n * std::exp(-(v - vs) * (v - vs) / (vth * vth * desc.marker_temp_ratio)) * std::pow(v2 / v, zeta)
            / (2 * M_PI * vth * vth * vth * marker_Ab * Bz * desc.marker_temp_ratio * std::sqrt(desc.marker_temp_ratio));

        REQUIRE(vdf.f0(ptl) == Catch::Approx{ f_ref }.epsilon(1e-10));
        REQUIRE(vdf.g0(ptl) == Catch::Approx{ g_ref }.epsilon(1e-10));
    }

    if constexpr (dump_samples) {
        static_assert(n_samples > 0);
        std::ofstream os{ "/Users/kyungguk/Downloads/PartialShellVDF-ani_shell.m" };
        os.setf(os.fixed);
        os.precision(20);
        println(os, '{');
        for (unsigned long i = 0; i < particles.size() - 1; ++i) {
            println(os, "    ", particles[i], ", ");
        }
        println(os, "    ", particles.back());
        println(os, '}');
        os.close();
    }
}
