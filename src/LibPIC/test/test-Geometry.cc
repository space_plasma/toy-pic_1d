/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <catch2/catch_all.hpp>

#define LIBPIC_INLINE_VERSION 1
#include <PIC/Geometry.h>
#include <cmath>
#include <functional>
#include <limits>

namespace {
template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Catch::Approx{ b.x }.margin(1e-15)
        && a.y == Catch::Approx{ b.y }.margin(1e-15)
        && a.z == Catch::Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::TensorTemplate<T1, T2> const &a, Detail::TensorTemplate<U1, U2> const &b) noexcept
{
    return a.lo() == b.lo() && a.hi() == b.hi();
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::FourVectorTemplate<T1, T2> const &a, Detail::FourVectorTemplate<U1, U2> const &b) noexcept
{
    return a.t == Catch::Approx{ b.t }.margin(1e-15) && a.s == b.s;
}
template <class T1, class T2, class T3, class U1, class U2, class U3>
[[nodiscard]] bool operator==(Detail::FourTensorTemplate<T1, T2, T3> const &a, Detail::FourTensorTemplate<U1, U2, U3> const &b) noexcept
{
    return a.tt == Catch::Approx{ b.tt }.margin(1e-15)
        && a.ts == b.ts
        && a.ss == b.ss;
}
} // namespace
using ::operator==;

TEST_CASE("Test LibPIC::Geometry::Exception", "[LibPIC::Geometry::Exception]")
{
    constexpr auto quiet_nan = std::numeric_limits<double>::quiet_NaN();

    REQUIRE_NOTHROW(Geometry());
    REQUIRE_THROWS_AS(Geometry(quiet_nan, 1, 1), std::exception);
    REQUIRE_THROWS_AS(Geometry(0, quiet_nan, 1), std::exception);
    REQUIRE_THROWS_AS(Geometry(0, 1, quiet_nan), std::exception);
    REQUIRE_THROWS_AS(Geometry(0, 0, 1), std::exception);
    REQUIRE_THROWS_AS(Geometry(0, 1, 0), std::exception);
}

TEST_CASE("Test LibPIC::Geometry::StraightGeometry", "[LibPIC::Geometry::StraightGeometry]")
{
    constexpr double theta     = 30 * M_PI / 180;
    constexpr double O0        = 2;
    constexpr auto   D         = Vector{ .5, 1, 1 };
    constexpr auto   pos_cart  = CartCoord{ 1 };
    constexpr auto   pos_curvi = CurviCoord{ 1 / D.x };

    auto const geo = Geometry{ theta, D.x, O0 };

    CHECK(geo.D() == D);
    CHECK(geo.D1() == D.x);
    CHECK(geo.D2() == D.y);
    CHECK(geo.D3() == D.z);

    CHECK(geo.inv_D() == 1 / D);
    CHECK(geo.inv_D1() == 1 / D.x);
    CHECK(geo.inv_D2() == 1 / D.y);
    CHECK(geo.inv_D3() == 1 / D.z);

    CHECK(geo.sqrt_g() == Catch::Approx{ D.fold(Real{ 1 }, std::multiplies{}) }.epsilon(1e-15));
    CHECK(geo.det_gij() == Catch::Approx{ geo.sqrt_g() * geo.sqrt_g() }.epsilon(1e-15));

    CHECK(geo.cotrans(pos_cart).q1 == Catch::Approx{ pos_curvi.q1 }.epsilon(1e-15));
    CHECK(geo.cotrans(pos_curvi).x == Catch::Approx{ pos_cart.x }.epsilon(1e-15));
}

TEST_CASE("Test LibPIC::Geometry::MFABasis", "[LibPIC::Geometry::MFABasis]")
{
    constexpr double theta     = 30 * M_PI / 180;
    constexpr double O0        = 2;
    constexpr auto   D         = Vector{ .5, 1, 1 };
    constexpr auto   pos_cart  = CartCoord{ 1 };
    constexpr auto   pos_curvi = CurviCoord{ 1 / D.x };
    constexpr auto   e1        = Vector{ 0.8660254038, +0.5, 0 };
    constexpr auto   e2        = Vector{ -0.5, 0.8660254038, 0 };
    constexpr auto   e3        = Vector{ 0, 0, 1 };

    auto const geo = Geometry{ theta, D.x, O0 };

    CHECK(geo.theta() == Catch::Approx{ theta }.epsilon(1e-15));

    CHECK(geo.Bmag_div_B0(pos_cart) == Catch::Approx{ 1 }.epsilon(1e-15));
    CHECK(geo.Bmag_div_B0(pos_curvi) == Catch::Approx{ 1 }.epsilon(1e-15));

    CHECK(geo.Bcontr_div_B0(pos_cart) == e1 / D);
    CHECK(geo.Bcontr_div_B0(pos_curvi) == e1 / D);

    CHECK(geo.Bcovar_div_B0(pos_cart) == e1 * D);
    CHECK(geo.Bcovar_div_B0(pos_curvi) == e1 * D);

    CHECK(geo.Bcart_div_B0(pos_cart) == e1);
    CHECK(geo.Bcart_div_B0(pos_curvi) == e1);

    CHECK(geo.e1(pos_cart) == e1);
    CHECK(geo.e1(pos_curvi) == e1);

    CHECK(geo.e2(pos_cart) == e2);
    CHECK(geo.e2(pos_curvi) == e2);

    CHECK(geo.e3(pos_cart) == e3);
    CHECK(geo.e3(pos_curvi) == e3);

    constexpr auto vmfa   = MFAVector{ 0.30, 0.5604, 0.493 };
    constexpr auto vvmfa  = MFATensor{ 0.30, 0.5604, 0.493, 0, 0, 0 };
    constexpr auto vcart  = CartVector{ -0.020392378860000004, 0.6353206362895201, 0.493 };
    constexpr auto vvcart = CartTensor{ 0.365100000008086, 0.495300000015105, 0.493, -0.11275650757476, 0, 0 };

    CHECK(geo.mfa_to_cart(vmfa, pos_cart) == vcart);
    CHECK(geo.mfa_to_cart(vmfa, pos_curvi) == vcart);
    CHECK(geo.mfa_to_cart(vvmfa, pos_cart) == vvcart);
    CHECK(geo.mfa_to_cart(vvmfa, pos_curvi) == vvcart);

    CHECK(geo.cart_to_mfa(vcart, pos_cart) == vmfa);
    CHECK(geo.cart_to_mfa(vcart, pos_curvi) == vmfa);
    CHECK(geo.cart_to_mfa(vvcart, pos_cart) == vvmfa);
    CHECK(geo.cart_to_mfa(vvcart, pos_curvi) == vvmfa);

    constexpr auto Vmfa   = FourMFAVector{ 1, vmfa };
    constexpr auto VVmfa  = FourMFATensor{ 1, vmfa, vvmfa };
    constexpr auto Vcart  = FourCartVector{ 1, vcart };
    constexpr auto VVcart = FourCartTensor{ 1, vcart, vvcart };

    CHECK(geo.mfa_to_cart(Vmfa, pos_cart) == Vcart);
    CHECK(geo.mfa_to_cart(Vmfa, pos_curvi) == Vcart);
    CHECK(geo.mfa_to_cart(VVmfa, pos_cart) == VVcart);
    CHECK(geo.mfa_to_cart(VVmfa, pos_curvi) == VVcart);

    CHECK(geo.cart_to_mfa(Vcart, pos_cart) == Vmfa);
    CHECK(geo.cart_to_mfa(Vcart, pos_curvi) == Vmfa);
    CHECK(geo.cart_to_mfa(VVcart, pos_cart) == VVmfa);
    CHECK(geo.cart_to_mfa(VVcart, pos_curvi) == VVmfa);
}

TEST_CASE("Test LibPIC::Geometry::CurviBasis", "[LibPIC::Geometry::CurviBasis]")
{
    constexpr double theta     = 30 * M_PI / 180;
    constexpr double O0        = 2;
    constexpr auto   D         = CartVector{ .5, 1, 1 };
    constexpr auto   pos_cart  = CartCoord{ 1 };
    constexpr auto   pos_curvi = CurviCoord{ 1 / D.x };

    constexpr auto covar_metric = Tensor{ D.x * D.x, D.y * D.y, D.z * D.z, 0, 0, 0 };
    constexpr auto contr_metric = Tensor{ 1 / (D.x * D.x), 1 / (D.y * D.y), 1 / (D.z * D.z), 0, 0, 0 };

    constexpr auto covar_basis1 = Vector{ D.x, 0, 0 };
    constexpr auto covar_basis2 = Vector{ 0, D.y, 0 };
    constexpr auto covar_basis3 = Vector{ 0, 0, D.z };
    constexpr auto covar_bases  = Tensor{ D.x, D.y, D.z, 0, 0, 0 };

    constexpr auto contr_basis1 = Vector{ 1 / D.x, 0, 0 };
    constexpr auto contr_basis2 = Vector{ 0, 1 / D.y, 0 };
    constexpr auto contr_basis3 = Vector{ 0, 0, 1 / D.z };
    constexpr auto contr_bases  = Tensor{ 1 / D.x, 1 / D.y, 1 / D.z, 0, 0, 0 };

    auto const geo = Geometry{ theta, D.x, O0 };

    CHECK(geo.covar_metric(pos_cart) == covar_metric);
    CHECK(geo.covar_metric(pos_curvi) == covar_metric);

    CHECK(geo.contr_metric(pos_cart) == contr_metric);
    CHECK(geo.contr_metric(pos_curvi) == contr_metric);

    CHECK(geo.covar_basis<0>(pos_cart) == covar_bases);
    CHECK(geo.covar_basis<1>(pos_cart) == covar_basis1);
    CHECK(geo.covar_basis<2>(pos_cart) == covar_basis2);
    CHECK(geo.covar_basis<3>(pos_cart) == covar_basis3);

    CHECK(geo.contr_basis<0>(pos_cart) == contr_bases);
    CHECK(geo.contr_basis<1>(pos_cart) == contr_basis1);
    CHECK(geo.contr_basis<2>(pos_cart) == contr_basis2);
    CHECK(geo.contr_basis<3>(pos_cart) == contr_basis3);

    CHECK(geo.covar_basis<0>(pos_curvi) == covar_bases);
    CHECK(geo.covar_basis<1>(pos_curvi) == covar_basis1);
    CHECK(geo.covar_basis<2>(pos_curvi) == covar_basis2);
    CHECK(geo.covar_basis<3>(pos_curvi) == covar_basis3);

    CHECK(geo.contr_basis<0>(pos_curvi) == contr_bases);
    CHECK(geo.contr_basis<1>(pos_curvi) == contr_basis1);
    CHECK(geo.contr_basis<2>(pos_curvi) == contr_basis2);
    CHECK(geo.contr_basis<3>(pos_curvi) == contr_basis3);

    constexpr auto vcart  = CartVector{ -0.020392378860000004, 0.6353206362895201, 0.493 };
    constexpr auto vcontr = ContrVector{ vcart / D };
    constexpr auto vcovar = CovarVector{ vcart * D };

    CHECK(geo.contr_to_covar(vcontr, pos_cart) == vcovar);
    CHECK(geo.covar_to_contr(vcovar, pos_cart) == vcontr);
    CHECK(geo.cart_to_contr(vcart, pos_cart) == vcontr);
    CHECK(geo.contr_to_cart(vcontr, pos_cart) == vcart);
    CHECK(geo.cart_to_covar(vcart, pos_cart) == vcovar);
    CHECK(geo.covar_to_cart(vcovar, pos_cart) == vcart);

    CHECK(geo.contr_to_covar(vcontr, pos_curvi) == vcovar);
    CHECK(geo.covar_to_contr(vcovar, pos_curvi) == vcontr);
    CHECK(geo.cart_to_contr(vcart, pos_curvi) == vcontr);
    CHECK(geo.contr_to_cart(vcontr, pos_curvi) == vcart);
    CHECK(geo.cart_to_covar(vcart, pos_curvi) == vcovar);
    CHECK(geo.covar_to_cart(vcovar, pos_curvi) == vcart);
}

TEST_CASE("Test LibPIC::Geometry::Geometry", "[LibPIC::Geometry::Geometry]")
{
    constexpr double theta     = 30 * M_PI / 180;
    constexpr double O0        = 2;
    constexpr auto   D         = CartVector{ .5, 1, 1 };
    constexpr auto   pos_cart  = CartCoord{ 1 };
    constexpr auto   pos_curvi = CurviCoord{ 1 / D.x };
    constexpr auto   Bcart     = CartVector{ 0.8660254038, +0.5, 0 } * O0;

    auto const geo = Geometry{ theta, D.x, O0 };

    CHECK(geo.is_valid(pos_curvi));

    CHECK(geo.B0() == Catch::Approx{ O0 }.epsilon(1e-15));

    CHECK(geo.Bmag(pos_cart) == Catch::Approx{ O0 }.epsilon(1e-15));
    CHECK(geo.Bmag(pos_curvi) == Catch::Approx{ O0 }.epsilon(1e-15));

    CHECK(geo.Bcart(pos_cart) == Bcart);
    CHECK(geo.Bcart(pos_curvi) == Bcart);

    CHECK(geo.Bcontr(pos_cart) == Bcart / D);
    CHECK(geo.Bcontr(pos_curvi) == Bcart / D);

    CHECK(geo.Bcovar(pos_cart) == Bcart * D);
    CHECK(geo.Bcovar(pos_curvi) == Bcart * D);
}
