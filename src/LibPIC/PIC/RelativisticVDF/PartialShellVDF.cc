/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "PartialShellVDF.h"
#include "../RandomReal.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>
#include <stdexcept>
#include <tuple>
#include <valarray>

LIBPIC_NAMESPACE_BEGIN(1)
RelativisticPartialShellVDF::Shell::Shell(unsigned const zeta, Real const xs)
: zeta{ zeta }, xs{ xs }
{
    Ab = .5 * (xs * std::exp(-xs * xs) + 2 / M_2_SQRTPI * (.5 + xs * xs) * std::erfc(-xs));
    Bz = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);
}
RelativisticPartialShellVDF::RelativisticPartialShellVDF(PartialShellPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c)
: RelativisticVDF{ geo, domain_extent, c }, desc{ desc }
{
    m_vth       = std::sqrt(desc.beta) * c * std::abs(desc.Oc) / desc.op;
    m_vth_cubed = m_vth * m_vth * m_vth;
    m_shell     = { desc.zeta, desc.vs / m_vth };
    if (desc.Vd >= c)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - Vd exceeding c" };
    auto const gd2 = c * c / ((c - desc.Vd) * (c + desc.Vd));
    m_gammad       = std::sqrt(gd2);
    m_betad        = desc.Vd / c;
    //
    m_marker_vth          = m_vth * std::sqrt(desc.marker_temp_ratio);
    m_marker_vth_cubed    = m_marker_vth * m_marker_vth * m_marker_vth;
    m_marker_shell        = { desc.zeta, desc.vs / m_marker_vth };
    m_Nrefcell_div_Ntotal = 1 / domain_extent.len;
    //
    m_particle_flux = lorentz_boost<-1>(particle_flux_vector_comoving(), m_betad, m_gammad);
    m_stress_energy = lorentz_boost<-1>(stress_energy_tensor_comoving(), m_betad, m_gammad);

    // initialize velocity integral table
    {
        constexpr auto t_max    = 5;
        auto const     xs       = m_marker_shell.xs;
        auto const     t_min    = -(xs < t_max ? xs : t_max);
        Range const    x_extent = { t_min + xs, t_max - t_min };
        // provisional extent
        Fv_extent.loc = Fv_of_x(x_extent.min());
        Fv_extent.len = Fv_of_x(x_extent.max()) - Fv_extent.loc;
        m_x_of_Fv     = init_integral_table(this, Fv_extent, x_extent, &RelativisticPartialShellVDF::Fv_of_x);
        // FIXME: Chopping the head and tail off is a hackish solution of fixing anomalous particle initialization close to the boundaries.
        m_x_of_Fv.erase(Fv_extent.min());
        m_x_of_Fv.erase(Fv_extent.max());
        // finalized extent
        Fv_extent.loc = m_x_of_Fv.begin()->first;
        Fv_extent.len = m_x_of_Fv.rbegin()->first - Fv_extent.loc;
    }
    // initialize pitch angle integral table
    {
        constexpr auto accuracy_goal = 10;
        auto const     ph_max        = std::acos(std::pow(10, -accuracy_goal / Real(desc.zeta + 1)));
        auto const     ph_min        = -ph_max;
        Range const    a_extent      = { ph_min + M_PI_2, ph_max - ph_min };
        // provisional extent
        Fa_extent.loc = Fa_of_a(a_extent.min());
        Fa_extent.len = Fa_of_a(a_extent.max()) - Fa_extent.loc;
        m_a_of_Fa     = init_integral_table(this, Fa_extent, a_extent, &RelativisticPartialShellVDF::Fa_of_a);
        // FIXME: Chopping the head and tail off is a hackish solution of fixing anomalous particle initialization close to the boundaries.
        m_a_of_Fa.erase(Fa_extent.min());
        m_a_of_Fa.erase(Fa_extent.max());
        // finalized extent
        Fa_extent.loc = m_a_of_Fa.begin()->first;
        Fa_extent.len = m_a_of_Fa.rbegin()->first - Fa_extent.loc;
    }
    // u1max = umax * cos(αmin)
    m_comoving_upar_max = m_marker_vth * rbegin(m_x_of_Fv)->second * std::abs(std::cos(begin(m_a_of_Fa)->second));
    m_L0_to_L_ratio_max = [this, gbeta1 = m_comoving_upar_max / c] {
        return m_gammad * (1 + std::abs(m_betad) * gbeta1 / std::sqrt(1 + gbeta1 * gbeta1));
    }();
}
auto RelativisticPartialShellVDF::init_integral_table(RelativisticPartialShellVDF const *self, Range const f_extent, Range const x_extent, Real (RelativisticPartialShellVDF::*f_of_x)(Real) const noexcept)
    -> std::map<Real, Real>
{
    std::map<Real, Real> table;
    table.insert_or_assign(end(table), f_extent.min(), x_extent.min());
    constexpr long n_samples    = 50000;
    constexpr long n_subsamples = 100;
    auto const     df           = f_extent.len / n_samples;
    auto const     dx           = x_extent.len / (n_samples * n_subsamples);
    Real           x            = x_extent.min();
    Real           f_current    = (self->*f_of_x)(x);
    for (long i = 1; i < n_samples; ++i) {
        Real const f_target = Real(i) * df + f_extent.min();
        while (f_current < f_target)
            f_current = (self->*f_of_x)(x += dx);
        table.insert_or_assign(end(table), f_current, x);
    }
    table.insert_or_assign(end(table), f_extent.max(), x_extent.max());
    return table;
}

auto RelativisticPartialShellVDF::Fa_of_a(Real const alpha) const noexcept -> Real
{
    static constexpr Real (*const int_cos_zeta)(unsigned, Real) noexcept = [](unsigned const zeta, Real const x) noexcept {
        if (zeta == 0)
            return x;
        if (zeta == 1)
            return std::sin(x);
        return std::pow(std::cos(x), zeta - 1) * std::sin(x) / zeta + Real(zeta - 1) / zeta * int_cos_zeta(zeta - 2, x);
    };
    return int_cos_zeta(desc.zeta + 1, alpha - M_PI_2);
}
auto RelativisticPartialShellVDF::Fv_of_x(Real const v_by_vth) const noexcept -> Real
{
    auto const xs = m_marker_shell.xs;
    auto const t  = v_by_vth - xs;
    return -(t + 2 * xs) * std::exp(-t * t) + 2 / M_2_SQRTPI * (.5 + xs * xs) * std::erf(t);
}
auto RelativisticPartialShellVDF::linear_interp(std::map<Real, Real> const &table, Real const x) -> Real
{
    auto const ub = table.upper_bound(x);
    if (ub == end(table) || ub == begin(table))
        throw std::out_of_range{ __PRETTY_FUNCTION__ };
    auto const &[x_min, y_min] = *std::prev(ub);
    auto const &[x_max, y_max] = *ub;
    return (y_min * (x_max - x) + y_max * (x - x_min)) / (x_max - x_min);
}

auto RelativisticPartialShellVDF::particle_flux_vector_comoving() const noexcept -> FourMFAVector
{
    // the ratio of n_comoving/n_lab
    auto const n0_comoving = 1 / m_gammad;
    return { n0_comoving * c, {} };
}
auto RelativisticPartialShellVDF::stress_energy_tensor_comoving() const noexcept -> FourMFATensor
{
    // define momentum space
    auto const linspace = [](Range const &ulim) {
        std::array<Real, 2000> us{};
        std::iota(begin(us), end(us), long{});
        auto const du = ulim.len / us.size();
        for (auto &u : us) {
            (u *= du) += ulim.min() + du / 2;
        }
        return us;
    };

    auto const ulim = [vth = m_vth, xs = m_shell.xs] {
        constexpr auto t_max = 5;
        auto const     t_min = -(xs < t_max ? xs : t_max);
        return Range{ t_min + xs, t_max - t_min } * vth;
    }();
    auto const uels = linspace(ulim);
    auto const duel = uels.at(2) - uels.at(1);

    auto const alim = [zeta = desc.zeta] {
        constexpr auto accuracy_goal = 10;
        auto const     ph_max        = std::acos(std::pow(10, -accuracy_goal / Real(zeta + 1)));
        auto const     ph_min        = -ph_max;
        return Range{ ph_min + M_PI_2, ph_max - ph_min };
    }();
    auto const alphas = linspace(alim);
    auto const dalpha = alphas.at(2) - alphas.at(1);

    // weight in the integrand
    auto const n0     = *particle_flux_vector_comoving().t / c;
    auto const weight = [this, n0, du = duel, da = dalpha](Real const u, Real const a) {
        auto const u1 = u * std::cos(a);
        auto const u2 = u * std::sin(a);
        return (2 * M_PI * (u * u * std::sin(a)) * du * da) * n0 * f_common(MFAVector{ u1, u2, 0 } / m_vth, m_shell, m_vth_cubed);
    };

    // evaluate integrand
    // 1. energy density       : ∫c    γ0c f0 du0
    // 2. c * momentum density : ∫c     u0 f0 du0, which is 0 in this case due to symmetry
    // 3. momentum flux        : ∫u0/γ0 u0 f0 du0
    auto const inner_loop = [c2 = this->c2, &weight, &alphas](Real const u) {
        std::valarray<FourMFATensor> integrand(alphas.size());
        std::transform(begin(alphas), end(alphas), begin(integrand), [&](Real const a) {
            auto const gamma = std::sqrt(1 + (u * u) / c2);
            auto const P1    = std::pow(u * std::cos(a), 2) / gamma;
            auto const P2    = .5 * std::pow(u * std::sin(a), 2) / gamma;
            return FourMFATensor{ gamma * c2, {}, { P1, P2, P2, 0, 0, 0 } } * weight(u, a);
        });
        return integrand.sum();
    };
    auto const outer_loop = [inner_loop, &uels] {
        std::valarray<FourMFATensor> integrand(uels.size());
        std::transform(begin(uels), end(uels), begin(integrand), [&](Real const u) {
            return inner_loop(u);
        });
        return integrand.sum();
    };

    return outer_loop();
}

auto RelativisticPartialShellVDF::f_common(MFAVector const &u0_by_vth, Shell const &shell, Real const denom) noexcept -> Real
{
    // note that the u0 is in co-moving frame and normalized by vth
    //
    // f0(x1, x2, x3) = exp(-(x - xs)^2)*sin^ζ(α)/(2π θ^3 A(xs) B(ζ))
    //
    auto const x    = std::sqrt(dot(u0_by_vth, u0_by_vth));
    auto const t    = x - shell.xs;
    Real const fv   = std::exp(-t * t) / shell.Ab;
    auto const mu   = u0_by_vth.x / x;
    auto const zeta = shell.zeta;
    Real const fa   = (zeta == 0 ? 1 : std::pow((1 - mu) * (1 + mu), .5 * zeta)) / shell.Bz;
    return .5 * fv * fa / (M_PI * denom);
}
auto RelativisticPartialShellVDF::f0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept -> Real
{
    // note that u = γ{v1, v2, v3} in lab frame, where γ = c/√(c^2 - v^2)
    // f(u1, u2, u3) = f0(γd(u1 - γVd), u2, u3)
    auto const shifted_gcgv = lorentz_boost<+1>(geomtr.cart_to_mfa(gcgvel, pos), m_betad, m_gammad);
    return Real{ this->n0(pos) } * f_common(shifted_gcgv.s / m_vth, m_shell, m_gammad * m_vth_cubed);
}
auto RelativisticPartialShellVDF::g0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept -> Real
{
    // note that u = γ{v1, v2, v3} in lab frame, where γ = c/√(c^2 - v^2)
    // f(u1, u2, u3) = f0(γd(u1 - γVd), u2, u3)
    auto const shifted_gcgv = lorentz_boost<+1>(geomtr.cart_to_mfa(gcgvel, pos), m_betad, m_gammad);
    return Real{ this->n0(pos) } * f_common(shifted_gcgv.s / m_marker_vth, m_marker_shell, m_gammad * m_marker_vth_cubed);
}

auto RelativisticPartialShellVDF::impl_emit(Badge<Super>, unsigned long const n) const -> std::vector<Particle>
{
    std::vector<Particle> ptls(n);
    std::generate(begin(ptls), end(ptls), [this] {
        return this->emit();
    });
    return ptls;
}
auto RelativisticPartialShellVDF::impl_emit(Badge<Super>) const -> Particle
{
    Particle ptl = load();

    switch (desc.scheme) {
        case ParticleScheme::full_f:
            ptl.psd        = { 1, f0(ptl), g0(ptl) };
            ptl.psd.weight = ptl.psd.real_f / ptl.psd.marker;
            break;
        case ParticleScheme::delta_f:
            ptl.psd = { desc.initial_weight, f0(ptl), g0(ptl) };
            ptl.psd.real_f += ptl.psd.weight * ptl.psd.marker; // f = f_0 + w*g
            break;
    }

    return ptl;
}
auto RelativisticPartialShellVDF::load() const -> Particle
{
    constexpr long max_tries = 1000;
    for (long i = 0; i < max_tries; ++i) {
        // energy-momentum four-vector in co-moving frame
        auto const gcgv_co = load_gcgv_comoving();

        using std::get;
        if (std::abs(get<1>(gcgv_co)) > m_comoving_upar_max)
            continue;

        // energy-momentum four-vector in lab frame
        auto const gcgv_mfa = lorentz_boost<-1>(gcgv_co, m_betad, m_gammad);

        // ratio of dx0 to dx, i.e., length contraction factor
        // NOTE: Since it's 1D, we just use γ_lab/γ_co version.
        if (auto const L0_to_L_ratio = get<0>(gcgv_mfa) / get<0>(gcgv_co);
            uniform_real<4736>() > Real{ L0_to_L_ratio } / m_L0_to_L_ratio_max)
            continue;

        // position in lab frame
        CurviCoord const pos{ bit_reversed<2>() * domain_extent.len + domain_extent.loc };

        return { geomtr.mfa_to_cart(gcgv_mfa, pos), pos };
    }
    throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ } + " - no particle candidate found before " + std::to_string(max_tries) + " tries" };
}
auto RelativisticPartialShellVDF::load_gcgv_comoving() const -> FourMFAVector
{
    // velocity in field-aligned, co-moving frame
    //
    Real const ph    = bit_reversed<5>() * 2 * M_PI; // [0, 2pi]
    Real const alpha = a_of_Fa(bit_reversed<3>() * Fa_extent.len + Fa_extent.loc);
    Real const u_vth = x_of_Fv(uniform_real<100>() * Fv_extent.len + Fv_extent.loc);
    Real const x1    = std::cos(alpha) * u_vth;
    Real const tmp   = std::sin(alpha) * u_vth;
    Real const x2    = std::cos(ph) * tmp;
    Real const x3    = std::sin(ph) * tmp;

    // boost from particle reference frame to co-moving frame
    return lorentz_boost<-1>(FourMFAVector{ c, {} }, MFAVector{ x1, x2, x3 } * (m_marker_vth / c));
}
LIBPIC_NAMESPACE_END(1)
