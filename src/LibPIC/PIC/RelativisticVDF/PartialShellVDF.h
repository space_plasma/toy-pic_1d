/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/RelativisticVDF.h>

#include <map>

LIBPIC_NAMESPACE_BEGIN(1)
/// Relativistic partial shell velocity distribution function
/// \details
/// In the co-moving frame,
/// f0(u, α) = exp(-(x - xs)^2)*sin^ζ(α)/(2π θ^3 A(xs) B(ζ)),
///
/// where u = γv, x = u/θ;
/// A(b) = (1/2) * (b exp(-b^2) + √π (1/2 + b^2) erfc(-b));
/// B(ζ) = √π Γ(1 + ζ/2)/Γ(1.5 + ζ/2).
///
/// In the lab frame,
/// f(u1, u2) = f0(γd(u1 - γVd), u2),
/// where γd = 1/√(1 - Vd^2/c^2) and γ = 1/√(1 - v^2/c^2).
///
class RelativisticPartialShellVDF : public RelativisticVDF<RelativisticPartialShellVDF> {
    using Super = RelativisticVDF<RelativisticPartialShellVDF>;

    struct Shell {
        unsigned zeta;
        Real     xs; // vs normalized by vth
        Real     Ab; // normalization constant associated with velocity distribution
        Real     Bz; // normalization constant associated with pitch angle distribution

        Shell() noexcept = default;
        Shell(unsigned zeta, Real vs_by_vth);
    };

    PartialShellPlasmaDesc desc;
    //
    Real  m_vth;
    Real  m_vth_cubed;
    Shell m_shell;
    Real  m_gammad; //!< γd.
    Real  m_betad;  //!< Vd/c.
    // marker psd parallel thermal speed
    Real  m_marker_vth;
    Real  m_marker_vth_cubed;
    Shell m_marker_shell;
    Real  m_Nrefcell_div_Ntotal;
    //
    FourMFAVector m_particle_flux;
    FourMFATensor m_stress_energy;
    //
    Real m_comoving_upar_max;
    Real m_L0_to_L_ratio_max;
    //
    Range                Fv_extent;
    Range                Fa_extent;
    std::map<Real, Real> m_x_of_Fv;
    std::map<Real, Real> m_a_of_Fa;

    [[nodiscard]] static auto init_integral_table(RelativisticPartialShellVDF const *self, Range f_extent, Range x_extent, Real (RelativisticPartialShellVDF::*f_of_x)(Real) const noexcept) -> std::map<Real, Real>;
    [[nodiscard]] static Real linear_interp(std::map<Real, Real> const &table, Real x);
    [[nodiscard]] inline Real x_of_Fv(Real const Fv) const { return linear_interp(m_x_of_Fv, Fv); }
    [[nodiscard]] inline Real a_of_Fa(Real const Fa) const { return linear_interp(m_a_of_Fa, Fa); }
    [[nodiscard]] Real        Fa_of_a(Real) const noexcept;
    [[nodiscard]] Real        Fv_of_x(Real) const noexcept;

public:
    /// Construct a relativistic partial shell distribution
    /// \note Necessary parameter check is assumed to be done already.
    /// \param desc A PartialShellPlasmaDesc object.
    /// \param geo A geometry object.
    /// \param domain_extent Spatial domain extent.
    /// \param c Light speed. A positive real.
    ///
    RelativisticPartialShellVDF(PartialShellPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c);

    // VDF interfaces
    //
    [[nodiscard]] inline decltype(auto) impl_plasma_desc(Badge<Super>) const noexcept { return (this->desc); }

    [[nodiscard]] inline auto impl_n(Badge<Super>, CurviCoord const &) const -> Scalar
    {
        return m_particle_flux.t / c;
    }
    [[nodiscard]] inline auto impl_nV(Badge<Super>, CurviCoord const &pos) const -> CartVector
    {
        return geomtr.mfa_to_cart(m_particle_flux.s, pos);
    }
    [[nodiscard]] inline auto impl_nuv(Badge<Super>, CurviCoord const &pos) const -> FourCartTensor
    {
        return geomtr.mfa_to_cart(m_stress_energy, pos);
    }

    [[nodiscard]] inline Real impl_Nrefcell_div_Ntotal(Badge<Super>) const { return m_Nrefcell_div_Ntotal; }
    [[nodiscard]] inline Real impl_f(Badge<Super>, Particle const &ptl) const { return f0(ptl); }

    [[nodiscard]] auto impl_emit(Badge<Super>, unsigned long) const -> std::vector<Particle>;
    [[nodiscard]] auto impl_emit(Badge<Super>) const -> Particle;

    // equilibrium physical distribution function
    //
    [[nodiscard]] Real f0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real f0(Particle const &ptl) const noexcept { return f0(ptl.gcgvel, ptl.pos); }

    // marker particle distribution function
    //
    [[nodiscard]] Real g0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real g0(Particle const &ptl) const noexcept { return g0(ptl.gcgvel, ptl.pos); }

private:
    [[nodiscard]] auto load() const -> Particle;
    [[nodiscard]] auto load_gcgv_comoving() const -> FourMFAVector;

    // particle flux four-vector in co-moving frame
    [[nodiscard]] auto particle_flux_vector_comoving() const noexcept -> FourMFAVector;
    // stress-energy four-tensor in co-moving frame
    [[nodiscard]] auto stress_energy_tensor_comoving() const noexcept -> FourMFATensor;

    // velocity is normalized by vth1 and shifted to drifting plasma frame
    [[nodiscard]] inline static auto f_common(MFAVector const &g_vel, Shell const &, Real denom) noexcept -> Real;
};
LIBPIC_NAMESPACE_END(1)
