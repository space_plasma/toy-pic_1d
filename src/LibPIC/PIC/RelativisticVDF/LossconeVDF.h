/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/RelativisticVDF.h>

LIBPIC_NAMESPACE_BEGIN(1)
/// Relativistic loss-cone velocity distribution function
/// \details
/// In the co-moving frame,
/// f0(u1, u2) = n0*exp(-x1^2)/(π^3/2 vth1 vth2^2) * ((1 - Δ*β)*exp(-x2^2) - (1 - Δ)*exp(-x2^2/β))/(1 - β),
///
/// where u = γv, x1 = u1/vth1, x2 = v2/vth2.
/// The effective temperature in the perpendicular direction is 2*T2/vth2^2 = 1 + (1 - Δ)*β
///
/// In the lab frame,
/// f(u1, u2) = f0(γd(u1 - γVd), u2),
/// where γd = 1/√(1 - Vd^2/c^2) and γ = 1/√(1 - v^2/c^2).
///
class RelativisticLossconeVDF : public RelativisticVDF<RelativisticLossconeVDF> {
    using Super = RelativisticVDF<RelativisticLossconeVDF>;

    struct RejectionSampler { // rejection sampler
        RejectionSampler() noexcept = default;
        RejectionSampler(Real Delta, Real beta /*must not be 1*/);
        [[nodiscard]] Real sample() const noexcept;
        [[nodiscard]] Real fOg(Real x) const noexcept; // ratio of the target to the proposed distributions
        //
        Real                  Delta;          //!< Δ parameter.
        Real                  beta;           //!< β parameter.
        Real                  alpha;          //!< thermal spread of of the proposed distribution
        Real                  M;              //!< the ratio f(x_pk)/g(x_pk)
        static constexpr Real a_offset = 0.3; //!< optimal value for thermal spread of the proposed distribution
    };

    LossconePlasmaDesc desc;
    RejectionSampler   m_rs;
    //
    Real m_vth1;         //!< Parallel thermal speed.
    Real m_vth1_cubed;   //!< vth1^3.
    Real m_xth2_squared; //!< The ratio of vth2^2 to vth1^2.
    Real m_gammad;       //!< γd.
    Real m_betad;        //!< Vd/c.
    // marker psd parallel thermal speed
    Real m_marker_vth1;
    Real m_marker_vth1_cubed;
    Real m_Nrefcell_div_Ntotal;
    //
    FourMFAVector m_particle_flux;
    FourMFATensor m_stress_energy;
    //
    Real m_comoving_upar_max;
    Real m_L0_to_L_ratio_max;

public:
    /// Construct a relativistic loss-cone distribution
    /// \note Necessary parameter check is assumed to be done already.
    /// \param desc A BiMaxPlasmaDesc object.
    /// \param geo A geometry object.
    /// \param domain_extent Spatial domain extent.
    /// \param c Light speed. A positive real.
    ///
    RelativisticLossconeVDF(LossconePlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c);

    // VDF interfaces
    //
    [[nodiscard]] inline decltype(auto) impl_plasma_desc(Badge<Super>) const noexcept { return (this->desc); }

    [[nodiscard]] inline auto impl_n(Badge<Super>, CurviCoord const &) const -> Scalar
    {
        return m_particle_flux.t / c;
    }
    [[nodiscard]] inline auto impl_nV(Badge<Super>, CurviCoord const &pos) const -> CartVector
    {
        return geomtr.mfa_to_cart(m_particle_flux.s, pos);
    }
    [[nodiscard]] inline auto impl_nuv(Badge<Super>, CurviCoord const &pos) const -> FourCartTensor
    {
        return geomtr.mfa_to_cart(m_stress_energy, pos);
    }

    [[nodiscard]] inline Real impl_Nrefcell_div_Ntotal(Badge<Super>) const { return m_Nrefcell_div_Ntotal; }
    [[nodiscard]] inline Real impl_f(Badge<Super>, Particle const &ptl) const { return f0(ptl); }

    [[nodiscard]] auto impl_emit(Badge<Super>, unsigned long) const -> std::vector<Particle>;
    [[nodiscard]] auto impl_emit(Badge<Super>) const -> Particle;

    // equilibrium physical distribution function
    //
    [[nodiscard]] Real f0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real f0(Particle const &ptl) const noexcept { return f0(ptl.gcgvel, ptl.pos); }

    // marker particle distribution function
    //
    [[nodiscard]] Real g0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real g0(Particle const &ptl) const noexcept { return g0(ptl.gcgvel, ptl.pos); }

private:
    [[nodiscard]] auto load() const -> Particle;
    [[nodiscard]] auto load_gcgv_comoving() const -> FourMFAVector;

    // particle flux four-vector in co-moving frame
    [[nodiscard]] auto particle_flux_vector_comoving() const noexcept -> FourMFAVector;
    // stress-energy four-tensor in co-moving frame
    [[nodiscard]] auto stress_energy_tensor_comoving() const noexcept -> FourMFATensor;

    // velocity is normalized by vth1 and shifted to drifting plasma frame
    [[nodiscard]] inline auto f_common(MFAVector const &g_vel, Real denom) const noexcept -> Real;
};
LIBPIC_NAMESPACE_END(1)
