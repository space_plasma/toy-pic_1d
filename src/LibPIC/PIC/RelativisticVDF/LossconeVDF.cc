/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "LossconeVDF.h"
#include "../RandomReal.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>
#include <stdexcept>
#include <tuple>
#include <valarray>

LIBPIC_NAMESPACE_BEGIN(1)
RelativisticLossconeVDF::RelativisticLossconeVDF(LossconePlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c)
: RelativisticVDF{ geo, domain_extent, c }, desc{ desc }
{ // parameter check is assumed to be done already
    Real const Delta = desc.losscone.Delta;
    Real const beta  = [beta = desc.losscone.beta]() noexcept { // avoid beta == 1
        constexpr Real eps  = 1e-5;
        Real const     diff = beta - 1;
        return beta + (std::abs(diff) < eps ? std::copysign(eps, diff) : 0);
    }();
    m_rs = RejectionSampler{ Delta, beta };
    //
    m_vth1         = std::sqrt(desc.beta1) * c * std::abs(desc.Oc) / desc.op;
    m_xth2_squared = desc.T2_T1 / (1 + (1 - Delta) * beta);
    m_vth1_cubed   = m_vth1 * m_vth1 * m_vth1;
    if (desc.Vd >= c)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - Vd exceeding c" };
    auto const gd2 = c * c / ((c - desc.Vd) * (c + desc.Vd));
    m_gammad       = std::sqrt(gd2);
    m_betad        = desc.Vd / c;
    //
    m_marker_vth1         = m_vth1 * std::sqrt(desc.marker_temp_ratio);
    m_marker_vth1_cubed   = m_marker_vth1 * m_marker_vth1 * m_marker_vth1;
    m_Nrefcell_div_Ntotal = 1 / domain_extent.len;
    //
    m_particle_flux = lorentz_boost<-1>(particle_flux_vector_comoving(), m_betad, m_gammad);
    m_stress_energy = lorentz_boost<-1>(stress_energy_tensor_comoving(), m_betad, m_gammad);
    //
    m_comoving_upar_max = 4 * m_marker_vth1;
    m_L0_to_L_ratio_max = [this, gbeta1 = m_comoving_upar_max / c] {
        return m_gammad * (1 + std::abs(m_betad) * gbeta1 / std::sqrt(1 + gbeta1 * gbeta1));
    }();
}
auto RelativisticLossconeVDF::particle_flux_vector_comoving() const noexcept -> FourMFAVector
{
    // the ratio of n_comoving/n_lab
    auto const n0_comoving = 1 / m_gammad;
    return { n0_comoving * c, {} };
}
auto RelativisticLossconeVDF::stress_energy_tensor_comoving() const noexcept -> FourMFATensor
{
    // define momentum space
    auto const u1max = m_vth1 * 4;
    auto const u1s   = [ulim = Range{ -1, 2 } * u1max] {
        std::array<Real, 2000> us{};
        std::iota(begin(us), end(us), long{});
        auto const du = ulim.len / us.size();
        for (auto &u : us) {
            (u *= du) += ulim.min() + du / 2;
        }
        return us;
    }();
    auto const du1 = u1s.at(1) - u1s.at(0);

    auto const u2max = m_vth1 * std::sqrt(m_xth2_squared * std::max(Real{ 1 }, m_rs.beta)) * 4.2;
    auto const u2s   = [ulim = Range{ 0, 1 } * u2max] {
        std::array<Real, 1500> us{};
        std::iota(begin(us), end(us), long{});
        auto const du = ulim.len / us.size();
        for (auto &u : us) {
            (u *= du) += ulim.min() + du / 2;
        }
        return us;
    }();
    auto const du2 = u2s.at(1) - u2s.at(0);

    // weight in the integrand
    auto const n0     = *particle_flux_vector_comoving().t / c;
    auto const weight = [this, n0, du1 = du1, du2 = du2](Real const u1, Real const u2) {
        return (2 * M_PI * u2 * du2 * du1) * n0 * f_common(MFAVector{ u1, u2, 0 } / m_vth1, m_vth1_cubed);
    };

    // evaluate integrand
    // 1. energy density       : ∫c    γ0c f0 du0
    // 2. c * momentum density : ∫c     u0 f0 du0, which is 0 in this case due to symmetry
    // 3. momentum flux        : ∫u0/γ0 u0 f0 du0
    auto const inner_loop = [c2 = this->c2, &weight, &u2s](Real const u1) {
        std::valarray<FourMFATensor> integrand(u2s.size());
        std::transform(begin(u2s), end(u2s), begin(integrand), [&](Real const u2) {
            auto const gamma = std::sqrt(1 + (u1 * u1 + u2 * u2) / c2);
            auto const P1    = u1 * u1 / gamma;
            auto const P2    = .5 * u2 * u2 / gamma;
            return FourMFATensor{ gamma * c2, {}, { P1, P2, P2, 0, 0, 0 } } * weight(u1, u2);
        });
        return integrand.sum();
    };
    auto const outer_loop = [inner_loop, &u1s] {
        std::valarray<FourMFATensor> integrand(u1s.size());
        std::transform(begin(u1s), end(u1s), begin(integrand), [&](Real const u1) {
            return inner_loop(u1);
        });
        return integrand.sum();
    };

    return outer_loop();
}

auto RelativisticLossconeVDF::f_common(MFAVector const &u0, Real const denom) const noexcept -> Real
{
    // note that the u0 is in co-moving frame and normalized by vth1
    //
    //                                  ((1 - Δ*β)*exp(-(x2^2 + x3^2)/xth2^2) - (1 - Δ)*exp(-(x2^2 + x3^2)/(β*xth2^2)))
    // f0(x1, x2, x3) = exp(-x1^2)/√π * -------------------------------------------------------------------------------
    //                                                             (π * xth2^2 * (1 - β))
    //
    Real const f1 = std::exp(-u0.x * u0.x) * M_2_SQRTPI * .5;
    Real const f2 = [D     = m_rs.Delta,
                     b     = m_rs.beta,
                     x2    = (u0.y * u0.y + u0.z * u0.z) / m_xth2_squared,
                     denom = M_PI * m_xth2_squared * (1 - m_rs.beta)]() noexcept {
        return ((1 - D * b) * std::exp(-x2) - (1 - D) * std::exp(-x2 / b)) / denom;
    }();
    return (f1 * f2) / denom;
}
auto RelativisticLossconeVDF::f0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept -> Real
{
    // note that u = γ{v1, v2, v3} in lab frame, where γ = c/√(c^2 - v^2)
    // f(u1, u2, u3) = f0(γd(u1 - γVd), u2, u3)
    auto const shifted_gcgv = lorentz_boost<+1>(geomtr.cart_to_mfa(gcgvel, pos), m_betad, m_gammad);
    return Real{ this->n0(pos) } * f_common(shifted_gcgv.s / m_vth1, m_gammad * m_vth1_cubed);
}
auto RelativisticLossconeVDF::g0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept -> Real
{
    // note that u = γ{v1, v2, v3} in lab frame, where γ = c/√(c^2 - v^2)
    // f(u1, u2, u3) = f0(γd(u1 - γVd), u2, u3)
    auto const shifted_gcgv = lorentz_boost<+1>(geomtr.cart_to_mfa(gcgvel, pos), m_betad, m_gammad);
    return Real{ this->n0(pos) } * f_common(shifted_gcgv.s / m_marker_vth1, m_gammad * m_marker_vth1_cubed);
}

auto RelativisticLossconeVDF::impl_emit(Badge<Super>, unsigned long const n) const -> std::vector<Particle>
{
    std::vector<Particle> ptls(n);
    std::generate(begin(ptls), end(ptls), [this] {
        return this->emit();
    });
    return ptls;
}
auto RelativisticLossconeVDF::impl_emit(Badge<Super>) const -> Particle
{
    Particle ptl = load();

    switch (desc.scheme) {
        case ParticleScheme::full_f:
            ptl.psd        = { 1, f0(ptl), g0(ptl) };
            ptl.psd.weight = ptl.psd.real_f / ptl.psd.marker;
            break;
        case ParticleScheme::delta_f:
            ptl.psd = { desc.initial_weight, f0(ptl), g0(ptl) };
            ptl.psd.real_f += ptl.psd.weight * ptl.psd.marker; // f = f_0 + w*g
            break;
    }

    return ptl;
}
auto RelativisticLossconeVDF::load() const -> Particle
{
    constexpr long max_tries = 1000;
    for (long i = 0; i < max_tries; ++i) {
        // energy-momentum four-vector in co-moving frame
        auto const gcgv_co = load_gcgv_comoving();

        using std::get;
        if (std::abs(get<1>(gcgv_co)) > m_comoving_upar_max)
            continue;

        // energy-momentum four-vector in lab frame
        auto const gcgv_mfa = lorentz_boost<-1>(gcgv_co, m_betad, m_gammad);

        // ratio of dx0 to dx, i.e., length contraction factor
        // NOTE: Since it's 1D, we just use γ_lab/γ_co version.
        if (auto const L0_to_L_ratio = get<0>(gcgv_mfa) / get<0>(gcgv_co);
            uniform_real<4736>() > Real{ L0_to_L_ratio } / m_L0_to_L_ratio_max)
            continue;

        // position in lab frame
        CurviCoord const pos{ bit_reversed<2>() * domain_extent.len + domain_extent.loc };

        return { geomtr.mfa_to_cart(gcgv_mfa, pos), pos };
    }
    throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ } + " - no particle candidate found before " + std::to_string(max_tries) + " tries" };
}
auto RelativisticLossconeVDF::load_gcgv_comoving() const -> FourMFAVector
{
    // velocity in field-aligned, co-moving frame (Hu et al., 2010, doi:10.1029/2009JA015158)
    //
    Real const phi1 = bit_reversed<3>() * 2 * M_PI;                               // [0, 2pi]
    Real const x1   = std::sqrt(-std::log(uniform_real<100>())) * std::sin(phi1); // v_para
    //
    Real const phi2 = bit_reversed<5>() * 2 * M_PI; // [0, 2pi]
    Real const tmp  = m_rs.sample() * std::sqrt(m_xth2_squared);
    Real const x2   = std::cos(phi2) * tmp; // in-plane γv_perp
    Real const x3   = std::sin(phi2) * tmp; // out-of-plane γv_perp

    // boost from particle reference frame to co-moving frame
    return lorentz_boost<-1>(FourMFAVector{ c, {} }, MFAVector{ x1, x2, x3 } * (m_marker_vth1 / c));
}

// MARK: - RejectionSampler
//
RelativisticLossconeVDF::RejectionSampler::RejectionSampler(Real const Delta, Real const beta /*must not be 1*/)
: Delta{ Delta }, beta{ beta }
{
    constexpr Real eps = 1e-5;
    if (std::abs(1 - Delta) < eps) { // Δ == 1
        alpha = 1;
        M     = 1;
    } else { // Δ != 1
        alpha               = (beta < 1 ? 1 : beta) + a_offset;
        auto const eval_xpk = [D = Delta, b = beta, a = alpha] {
            Real const det = -b / (1 - b) * std::log(((a - 1) * (1 - D * b) * b) / ((a - b) * (1 - D)));
            return std::isfinite(det) && det > 0 ? std::sqrt(det) : 0;
        };
        Real const xpk = std::abs(1 - Delta * beta) < eps ? 0 : eval_xpk();
        M              = fOg(xpk);
    }
    if (!std::isfinite(M))
        throw std::runtime_error{ __PRETTY_FUNCTION__ };
}
auto RelativisticLossconeVDF::RejectionSampler::fOg(const Real x) const noexcept -> Real
{
    using std::exp;
    Real const x2 = x * x;
    Real const f  = ((1 - Delta * beta) * exp(-x2) - (1 - Delta) * exp(-x2 / beta)) / (1 - beta);
    Real const g  = exp(-x2 / alpha) / alpha;
    return f / g; // ratio of the target distribution to proposed distribution
}
auto RelativisticLossconeVDF::RejectionSampler::sample() const noexcept -> Real
{
    auto const vote = [this](Real const proposal) noexcept {
        Real const jury = uniform_real<300>() * M;
        return jury <= fOg(proposal);
    };
    auto const proposed = [a = this->alpha]() noexcept {
        return std::sqrt(-std::log(uniform_real<200>()) * a);
    };
    //
    Real sample;
    while (!vote(sample = proposed())) {}
    return sample;
}
LIBPIC_NAMESPACE_END(1)
