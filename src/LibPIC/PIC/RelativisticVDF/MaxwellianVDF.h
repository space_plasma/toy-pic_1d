/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/RelativisticVDF.h>

LIBPIC_NAMESPACE_BEGIN(1)
/// Relativistic bi-Maxwellian velocity distribution function
/// \details
/// In the co-moving frame,
/// f0(u1, u2) = n0*exp(-x1^2 -x2^2)/(π^3/2 vth1^3 T2/T1),
///
/// where u = γv, x1 = u1/vth1, x2 = u2/(vth1*√(T2/T1))), and
/// T2 and T1 are temperatures in the directions perpendicular and
/// parallel to the background magnetic field direction, respectively.
///
/// In the lab frame,
/// f(u1, u2) = f0(γd(u1 - γVd), u2),
/// where γd = 1/√(1 - Vd^2/c^2) and γ = 1/√(1 - v^2/c^2).
///
class RelativisticMaxwellianVDF : public RelativisticVDF<RelativisticMaxwellianVDF> {
    using Super = RelativisticVDF<RelativisticMaxwellianVDF>;

    BiMaxPlasmaDesc desc;
    //
    Real m_vth1; //!< Parallel thermal speed.
    Real m_vth1_cubed;
    Real m_gammad; //!< γd.
    Real m_betad;  //!< Vd/c.
    // marker psd parallel thermal speed
    Real m_marker_vth1;
    Real m_marker_vth1_cubed;
    Real m_Nrefcell_div_Ntotal;
    //
    FourMFAVector m_particle_flux;
    FourMFATensor m_stress_energy;
    //
    Real m_comoving_upar_max;
    Real m_L0_to_L_ratio_max;

public:
    /// Construct a relativistic bi-Maxwellian distribution
    /// \note Necessary parameter check is assumed to be done already.
    /// \param desc A BiMaxPlasmaDesc object.
    /// \param geo A geometry object.
    /// \param domain_extent Spatial domain extent.
    /// \param c Light speed. A positive real.
    ///
    RelativisticMaxwellianVDF(BiMaxPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c);

    // VDF interfaces
    //
    [[nodiscard]] inline decltype(auto) impl_plasma_desc(Badge<Super>) const noexcept { return (this->desc); }

    [[nodiscard]] inline auto impl_n(Badge<Super>, CurviCoord const &) const -> Scalar
    {
        return m_particle_flux.t / c;
    }
    [[nodiscard]] inline auto impl_nV(Badge<Super>, CurviCoord const &pos) const -> CartVector
    {
        return geomtr.mfa_to_cart(m_particle_flux.s, pos);
    }
    [[nodiscard]] inline auto impl_nuv(Badge<Super>, CurviCoord const &pos) const -> FourCartTensor
    {
        return geomtr.mfa_to_cart(m_stress_energy, pos);
    }

    [[nodiscard]] inline Real impl_Nrefcell_div_Ntotal(Badge<Super>) const { return m_Nrefcell_div_Ntotal; }
    [[nodiscard]] inline Real impl_f(Badge<Super>, Particle const &ptl) const { return f0(ptl); }

    [[nodiscard]] auto impl_emit(Badge<Super>, unsigned long) const -> std::vector<Particle>;
    [[nodiscard]] auto impl_emit(Badge<Super>) const -> Particle;

    // equilibrium physical distribution function
    //
    [[nodiscard]] Real f0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real f0(Particle const &ptl) const noexcept { return f0(ptl.gcgvel, ptl.pos); }

    // marker particle distribution function
    //
    [[nodiscard]] Real g0(FourCartVector const &gcgvel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real g0(Particle const &ptl) const noexcept { return g0(ptl.gcgvel, ptl.pos); }

private:
    [[nodiscard]] auto load() const -> Particle;
    [[nodiscard]] auto load_gcgv_comoving() const -> FourMFAVector;

    // particle flux four-vector in co-moving frame
    [[nodiscard]] auto particle_flux_vector_comoving() const noexcept -> FourMFAVector;
    // stress-energy four-tensor in co-moving frame
    [[nodiscard]] auto stress_energy_tensor_comoving() const noexcept -> FourMFATensor;

    // velocity is normalized by vth1 and shifted to drifting plasma frame
    [[nodiscard]] inline static auto f_common(MFAVector const &g_vel, Real T2OT1, Real denom) noexcept -> Real;
};
LIBPIC_NAMESPACE_END(1)
