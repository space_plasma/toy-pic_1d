/*
 * Copyright (c) 2019-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Geometry.h"

#include <cmath>
#include <limits>
#include <stdexcept>

LIBPIC_NAMESPACE_BEGIN(1)
namespace {
constexpr auto quiet_nan = std::numeric_limits<Real>::quiet_NaN();
}
Geometry::Geometry() noexcept
: m_O0{ quiet_nan }
{
}

Geometry::Geometry(Real const theta, Real const D1, Real const O0)
: UniformGeometry{ theta, D1 }, m_O0{ O0 }
{
    if (!std::isfinite(theta))
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - theta is not a finite number" };

    if (!std::isfinite(D1) || D1 <= 0)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - D1 is non-positive" };

    if (!std::isfinite(O0) || O0 <= 0)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - O0 is non-positive" };
}
LIBPIC_NAMESPACE_END(1)
