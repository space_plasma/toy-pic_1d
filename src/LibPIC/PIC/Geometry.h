/*
 * Copyright (c) 2019-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/Config.h>
#include <PIC/Geometry/UniformGeometry.h>
#include <PIC/Predefined.h>

LIBPIC_NAMESPACE_BEGIN(1)
class Geometry
: public Detail::UniformGeometry {
    Real m_O0;

public:
    Geometry() noexcept;
    Geometry(Real theta, Real D1, Real O0);

    /// Check if the curvilinear coordinate is within the valid range
    ///
    [[nodiscard]] static bool is_valid(CurviCoord const &) noexcept { return true; }

    /// Magnitude of B at the origin
    [[nodiscard]] auto &B0() const noexcept { return m_O0; }

    /// Contravariant components of B
    template <class Coord>
    [[nodiscard]] decltype(auto) Bcontr(Coord const &pos) const noexcept { return Bcontr_div_B0(pos) * B0(); }

    /// Covariant components of B
    template <class Coord>
    [[nodiscard]] decltype(auto) Bcovar(Coord const &pos) const noexcept { return Bcovar_div_B0(pos) * B0(); }

    /// Cartesian components of B
    template <class Coord>
    [[nodiscard]] decltype(auto) Bcart(Coord const &pos) const noexcept { return Bcart_div_B0(pos) * B0(); }

    /// Magnitude of B/B0
    template <class Coord>
    [[nodiscard]] decltype(auto) Bmag(Coord const &pos) const noexcept { return Bmag_div_B0(pos) * B0(); }
};
LIBPIC_NAMESPACE_END(1)
