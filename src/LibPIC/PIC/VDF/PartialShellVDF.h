/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/VDF.h>

#include <map>

LIBPIC_NAMESPACE_BEGIN(1)
/// Partial shell velocity distribution function
/// \details
/// f(v, α) = exp(-(x - xs)^2)*sin^ζ(α)/(2π θ^3 A(xs) B(ζ)),
///
/// where x = (v - vd)/θ;
/// A(b) = (1/2) * (b exp(-b^2) + √π (1/2 + b^2) erfc(-b));
/// B(ζ) = √π Γ(1 + ζ/2)/Γ(1.5 + ζ/2).
///
class PartialShellVDF : public VDF<PartialShellVDF> {
    using Super = VDF<PartialShellVDF>;

    struct Shell {
        unsigned zeta;
        Real     xs;         // vs normalized by vth
        Real     Ab;         // normalization constant associated with velocity distribution
        Real     Bz;         // normalization constant associated with pitch angle distribution
        Real     T1_by_vth2; // parallel temperature normalized by thermal speed squared

        Shell() noexcept = default;
        Shell(unsigned zeta, Real vs_by_vth);
    };

    PartialShellPlasmaDesc desc;
    //
    Real  m_vth;
    Real  m_vth_cubed;
    Shell m_shell;
    // marker psd parallel thermal speed
    Real  m_marker_vth;
    Real  m_marker_vth_cubed;
    Shell m_marker_shell;
    Real  m_Nrefcell_div_Ntotal;
    //
    MFAVector m_Vd;
    MFATensor m_vv;
    //
    Range                Fv_extent;
    Range                Fa_extent;
    std::map<Real, Real> m_x_of_Fv;
    std::map<Real, Real> m_a_of_Fa;

    [[nodiscard]] static auto init_integral_table(PartialShellVDF const *self, Range f_extent, Range x_extent, Real (PartialShellVDF::*f_of_x)(Real) const noexcept) -> std::map<Real, Real>;
    [[nodiscard]] static Real linear_interp(std::map<Real, Real> const &table, Real x);
    [[nodiscard]] inline Real x_of_Fv(Real const Fv) const { return linear_interp(m_x_of_Fv, Fv); }
    [[nodiscard]] inline Real a_of_Fa(Real const Fa) const { return linear_interp(m_a_of_Fa, Fa); }
    [[nodiscard]] Real        Fa_of_a(Real) const noexcept;
    [[nodiscard]] Real        Fv_of_x(Real) const noexcept;

public:
    /// Construct a partial shell distribution
    /// \note Necessary parameter check is assumed to be done already.
    /// \param desc A PartialShellPlasmaDesc object.
    /// \param geo A geometry object.
    /// \param domain_extent Spatial domain extent.
    /// \param c Light speed. A positive real.
    ///
    PartialShellVDF(PartialShellPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c);

    // VDF interfaces
    //
    [[nodiscard]] inline decltype(auto) impl_plasma_desc(Badge<Super>) const noexcept { return (this->desc); }

    [[nodiscard]] inline auto impl_n(Badge<Super>, CurviCoord const &) const { return Scalar{ 1 }; }
    [[nodiscard]] inline auto impl_nV(Badge<Super>, CurviCoord const &pos) const -> CartVector
    {
        return geomtr.mfa_to_cart(m_Vd, pos) * Real{ this->n0(pos) };
    }
    [[nodiscard]] inline auto impl_nvv(Badge<Super>, CurviCoord const &pos) const -> CartTensor
    {
        return geomtr.mfa_to_cart(m_vv, pos) * Real{ this->n0(pos) };
    }

    [[nodiscard]] inline Real impl_Nrefcell_div_Ntotal(Badge<Super>) const { return m_Nrefcell_div_Ntotal; }
    [[nodiscard]] inline Real impl_f(Badge<Super>, Particle const &ptl) const { return f0(ptl); }

    [[nodiscard]] auto impl_emit(Badge<Super>, unsigned long) const -> std::vector<Particle>;
    [[nodiscard]] auto impl_emit(Badge<Super>) const -> Particle;

    // equilibrium physical distribution function
    //
    [[nodiscard]] Real f0(CartVector const &vel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real f0(Particle const &ptl) const noexcept { return f0(ptl.vel, ptl.pos); }

    // marker particle distribution function
    //
    [[nodiscard]] Real g0(CartVector const &vel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real g0(Particle const &ptl) const noexcept { return g0(ptl.vel, ptl.pos); }

private:
    [[nodiscard]] Particle load() const;

    // velocity is normalized by vth and shifted to drifting plasma frame
    [[nodiscard]] inline static auto f_common(MFAVector const &vel, Shell const &) noexcept;
};
LIBPIC_NAMESPACE_END(1)
