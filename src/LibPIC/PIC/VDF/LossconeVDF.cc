/*
 * Copyright (c) 2020-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "LossconeVDF.h"
#include "../RandomReal.h"
#include <cmath>
#include <stdexcept>

LIBPIC_NAMESPACE_BEGIN(1)
LossconeVDF::LossconeVDF(LossconePlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c)
: VDF{ geo, domain_extent }, desc{ desc }
{ // parameter check is assumed to be done already
    Real const Delta = desc.losscone.Delta;
    Real const beta  = [beta = desc.losscone.beta]() noexcept { // avoid beta == 1
        constexpr Real eps  = 1e-5;
        Real const     diff = beta - 1;
        return beta + (std::abs(diff) < eps ? std::copysign(eps, diff) : 0);
    }();
    m_rs = RejectionSampler{ Delta, beta };
    //
    m_vth1         = std::sqrt(desc.beta1) * c * std::abs(desc.Oc) / desc.op;
    m_xth2_squared = desc.T2_T1 / (1 + (1 - Delta) * beta);
    m_vth1_cubed   = m_vth1 * m_vth1 * m_vth1;
    //
    m_marker_vth1         = m_vth1 * std::sqrt(desc.marker_temp_ratio);
    m_marker_vth1_cubed   = m_marker_vth1 * m_marker_vth1 * m_marker_vth1;
    m_Nrefcell_div_Ntotal = 1 / domain_extent.len;
    //
    m_Vd = { desc.Vd, 0, 0 }; // bulk velocity in field-aligned coordinates
    m_vv = [xd = desc.Vd / m_vth1, T2OT1 = desc.T2_T1](Real const scale) {
        return MFATensor{ 1 + 2 * xd * xd, T2OT1, T2OT1, 0, 0, 0 } * scale;
    }(.5 * m_vth1 * m_vth1); // 2nd moment divided by density in field-aligned coordinates
}

auto LossconeVDF::f_common(MFAVector const &v, Real const xth2_squared, RejectionSampler const &rs) noexcept
{
    // note that vel = {v1 - Vd, v2, v3}/vth1
    //
    // f0(x1, x2, x3) = exp(-(x1 - xd)^2)/√π *
    // ((1 - Δ*β)*exp(-(x2^2 + x3^2)/xth2^2) - (1 - Δ)*exp(-(x2^2 + x3^2)/(β*xth2^2)))
    // -------------------------------------------------------------------------------
    //                           (π * xth2^2 * (1 - β))
    //
    Real const f1 = std::exp(-v.x * v.x) * M_2_SQRTPI * .5;
    Real const f2 = [D     = rs.Delta,
                     b     = rs.beta,
                     x2    = (v.y * v.y + v.z * v.z) / xth2_squared,
                     denom = M_PI * xth2_squared * (1 - rs.beta)] {
        return ((1 - D * b) * std::exp(-x2) - (1 - D) * std::exp(-x2 / b)) / denom;
    }();
    return f1 * f2;
}
auto LossconeVDF::f0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_vth1, m_xth2_squared, m_rs) / m_vth1_cubed;
}
auto LossconeVDF::g0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_marker_vth1, m_xth2_squared, m_rs) / m_marker_vth1_cubed;
}

auto LossconeVDF::impl_emit(Badge<Super>, unsigned long const n) const -> std::vector<Particle>
{
    std::vector<Particle> ptls(n);
    std::generate(begin(ptls), end(ptls), [this] {
        return this->emit();
    });
    return ptls;
}
auto LossconeVDF::impl_emit(Badge<Super>) const -> Particle
{
    Particle ptl = load();

    switch (desc.scheme) {
        case ParticleScheme::full_f:
            ptl.psd        = { 1, f0(ptl), g0(ptl) };
            ptl.psd.weight = ptl.psd.real_f / ptl.psd.marker;
            break;
        case ParticleScheme::delta_f:
            ptl.psd = { desc.initial_weight, f0(ptl), g0(ptl) };
            ptl.psd.real_f += ptl.psd.weight * ptl.psd.marker; // f = f_0 + w*g
            break;
    }

    return ptl;
}
auto LossconeVDF::load() const -> Particle
{
    // position
    //
    CurviCoord const pos{ bit_reversed<2>() * domain_extent.len + domain_extent.loc };

    // velocity in field-aligned, co-moving frame (Hu et al., 2010, doi:10.1029/2009JA015158)
    //
    Real const phi1 = bit_reversed<3>() * 2 * M_PI;                               // [0, 2pi]
    Real const v1   = std::sqrt(-std::log(uniform_real<100>())) * std::sin(phi1); // v_para
    //
    Real const phi2   = bit_reversed<5>() * 2 * M_PI; // [0, 2pi]
    Real const tmp_v2 = m_rs.sample() * std::sqrt(m_xth2_squared);
    Real const v2     = std::cos(phi2) * tmp_v2; // in-plane v_perp
    Real const v3     = std::sin(phi2) * tmp_v2; // out-of-plane v_perp

    // velocity in field-aligned frame
    //
    auto const vel = MFAVector{ v1, v2, v3 } * m_marker_vth1 + m_Vd;

    return { geomtr.mfa_to_cart(vel, pos), pos };
}

// MARK: - RejectionSampler
//
LossconeVDF::RejectionSampler::RejectionSampler(Real const Delta, Real const beta /*must not be 1*/)
: Delta{ Delta }, beta{ beta }
{
    constexpr Real eps = 1e-5;
    if (std::abs(1 - Delta) < eps) { // Δ == 1
        alpha = 1;
        M     = 1;
    } else { // Δ != 1
        alpha               = (beta < 1 ? 1 : beta) + a_offset;
        auto const eval_xpk = [D = Delta, b = beta, a = alpha] {
            Real const det = -b / (1 - b) * std::log(((a - 1) * (1 - D * b) * b) / ((a - b) * (1 - D)));
            return std::isfinite(det) && det > 0 ? std::sqrt(det) : 0;
        };
        Real const xpk = std::abs(1 - Delta * beta) < eps ? 0 : eval_xpk();
        M              = fOg(xpk);
    }
    if (!std::isfinite(M))
        throw std::runtime_error{ __PRETTY_FUNCTION__ };
}
auto LossconeVDF::RejectionSampler::fOg(const Real x) const noexcept -> Real
{
    using std::exp;
    Real const x2 = x * x;
    Real const f  = ((1 - Delta * beta) * exp(-x2) - (1 - Delta) * exp(-x2 / beta)) / (1 - beta);
    Real const g  = exp(-x2 / alpha) / alpha;
    return f / g; // ratio of the target distribution to proposed distribution
}
auto LossconeVDF::RejectionSampler::sample() const noexcept -> Real
{
    auto const vote = [this](Real const proposal) noexcept {
        Real const jury = uniform_real<300>() * M;
        return jury <= fOg(proposal);
    };
    auto const proposed = [a = this->alpha]() noexcept {
        return std::sqrt(-std::log(uniform_real<200>()) * a);
    };
    //
    Real sample;
    while (!vote(sample = proposed())) {}
    return sample;
}
LIBPIC_NAMESPACE_END(1)
