/*
 * Copyright (c) 2019-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "MaxwellianVDF.h"
#include "../RandomReal.h"
#include <algorithm>
#include <cmath>

LIBPIC_NAMESPACE_BEGIN(1)
MaxwellianVDF::MaxwellianVDF(BiMaxPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c)
: VDF{ geo, domain_extent }, desc{ desc }
{ // parameter check is assumed to be done already
    m_vth1       = std::sqrt(desc.beta1) * c * std::abs(desc.Oc) / desc.op;
    m_T2OT1      = desc.T2_T1;
    m_vth1_cubed = m_vth1 * m_vth1 * m_vth1;
    //
    m_marker_vth1         = m_vth1 * std::sqrt(desc.marker_temp_ratio);
    m_marker_vth1_cubed   = m_marker_vth1 * m_marker_vth1 * m_marker_vth1;
    m_Nrefcell_div_Ntotal = 1 / domain_extent.len;
    //
    m_Vd = { desc.Vd, 0, 0 }; // bulk velocity in field-aligned coordinates
    m_vv = [xd = desc.Vd / m_vth1, T2OT1 = m_T2OT1](Real const scale) {
        return MFATensor{ 1 + 2 * xd * xd, T2OT1, T2OT1, 0, 0, 0 } * scale;
    }(.5 * m_vth1 * m_vth1); // 2nd moment divided by density in field-aligned coordinates
}

auto MaxwellianVDF::f_common(MFAVector const &v, Real const T2OT1) noexcept
{
    // note that vel = {v1 - Vd, v2, v3}/vth1
    // f0(x1, x2, x3) = exp(-(x1 - xd)^2)/√π * exp(-(x2^2 + x3^2)/(T2/T1))/(π T2/T1)
    //
    Real const f1 = std::exp(-v.x * v.x) * M_2_SQRTPI * .5;
    Real const x2 = v.y * v.y + v.z * v.z;
    Real const f2 = std::exp(-x2 / T2OT1) / (M_PI * T2OT1);
    return f1 * f2;
}
auto MaxwellianVDF::f0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_vth1, m_T2OT1) / m_vth1_cubed;
}
auto MaxwellianVDF::g0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_marker_vth1, m_T2OT1) / m_marker_vth1_cubed;
}

auto MaxwellianVDF::impl_emit(Badge<Super>, unsigned long const n) const -> std::vector<Particle>
{
    std::vector<Particle> ptls(n);
    std::generate(begin(ptls), end(ptls), [this] {
        return this->emit();
    });
    return ptls;
}
auto MaxwellianVDF::impl_emit(Badge<Super>) const -> Particle
{
    Particle ptl = load();

    switch (desc.scheme) {
        case ParticleScheme::full_f:
            ptl.psd        = { 1, f0(ptl), g0(ptl) };
            ptl.psd.weight = ptl.psd.real_f / ptl.psd.marker;
            break;
        case ParticleScheme::delta_f:
            ptl.psd = { desc.initial_weight, f0(ptl), g0(ptl) };
            ptl.psd.real_f += ptl.psd.weight * ptl.psd.marker; // f = f_0 + w*g
            break;
    }

    return ptl;
}
auto MaxwellianVDF::load() const -> Particle
{
    // position
    //
    CurviCoord const pos{ bit_reversed<2>() * domain_extent.len + domain_extent.loc };

    // velocity in field-aligned, moving frame (Hu et al., 2010, doi:10.1029/2009JA015158)
    //
    Real const phi1 = bit_reversed<3>() * 2 * M_PI;                               // [0, 2pi]
    Real const v1   = std::sqrt(-std::log(uniform_real<100>())) * std::sin(phi1); // v_para
    //
    Real const phi2   = bit_reversed<5>() * 2 * M_PI; // [0, 2pi]
    Real const tmp_v2 = std::sqrt(-std::log(uniform_real<200>()) * m_T2OT1);
    Real const v2     = std::cos(phi2) * tmp_v2; // in-plane v_perp
    Real const v3     = std::sin(phi2) * tmp_v2; // out-of-plane v_perp

    // velocity in field-aligned frame
    //
    auto const vel = MFAVector{ v1, v2, v3 } * m_marker_vth1 + m_Vd;

    return { geomtr.mfa_to_cart(vel, pos), pos };
}
LIBPIC_NAMESPACE_END(1)
