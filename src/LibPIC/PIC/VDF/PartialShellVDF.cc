/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "PartialShellVDF.h"
#include "../RandomReal.h"
#include <algorithm>
#include <cmath>
#include <stdexcept>

LIBPIC_NAMESPACE_BEGIN(1)
PartialShellVDF::Shell::Shell(unsigned const zeta, Real const xs)
: zeta{ zeta }, xs{ xs }
{
    Ab = .5 * (xs * std::exp(-xs * xs) + 2 / M_2_SQRTPI * (.5 + xs * xs) * std::erfc(-xs));
    Bz = 2 / M_2_SQRTPI * std::tgamma(1 + .5 * zeta) / std::tgamma(1.5 + .5 * zeta);

    auto const T_vth2 = .5 / Ab * (xs * (2.5 + xs * xs) * std::exp(-xs * xs) + 2 / M_2_SQRTPI * (0.75 + xs * xs * (3 + xs * xs)) * std::erfc(-xs));
    T1_by_vth2        = T_vth2 / Real(3 + zeta);
}
PartialShellVDF::PartialShellVDF(PartialShellPlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c)
: VDF{ geo, domain_extent }, desc{ desc }
{
    m_vth           = std::sqrt(desc.beta) * c * std::abs(desc.Oc) / desc.op;
    auto const vth2 = m_vth * m_vth;
    m_vth_cubed     = vth2 * m_vth;
    m_shell         = { desc.zeta, desc.vs / m_vth };
    //
    m_marker_vth          = m_vth * std::sqrt(desc.marker_temp_ratio);
    m_marker_vth_cubed    = m_marker_vth * m_marker_vth * m_marker_vth;
    m_marker_shell        = { desc.zeta, desc.vs / m_marker_vth };
    m_Nrefcell_div_Ntotal = 1 / domain_extent.len;
    //
    m_Vd = { desc.Vd, 0, 0 }; // bulk velocity in field-aligned coordinates
    auto const T1
        = m_shell.T1_by_vth2 * m_vth * m_vth;
    m_vv = [xd2 = desc.Vd * desc.Vd / T1, T2OT1 = Real(2 + desc.zeta) / 2](Real const scale) {
        return MFATensor{ 1 + xd2, T2OT1, T2OT1, 0, 0, 0 } * scale;
    }(T1); // 2nd moment divided by density in field-aligned coordinates

    // initialize velocity integral table
    {
        constexpr auto t_max    = 5;
        auto const     xs       = m_marker_shell.xs;
        auto const     t_min    = -(xs < t_max ? xs : t_max);
        Range const    x_extent = { t_min + xs, t_max - t_min };
        // provisional extent
        Fv_extent.loc = Fv_of_x(x_extent.min());
        Fv_extent.len = Fv_of_x(x_extent.max()) - Fv_extent.loc;
        m_x_of_Fv     = init_integral_table(this, Fv_extent, x_extent, &PartialShellVDF::Fv_of_x);
        // FIXME: Chopping the head and tail off is a hackish solution of fixing anomalous particle initialization close to the boundaries.
        m_x_of_Fv.erase(Fv_extent.min());
        m_x_of_Fv.erase(Fv_extent.max());
        // finalized extent
        Fv_extent.loc = m_x_of_Fv.begin()->first;
        Fv_extent.len = m_x_of_Fv.rbegin()->first - Fv_extent.loc;
    }
    // initialize pitch angle integral table
    {
        constexpr auto accuracy_goal = 10;
        auto const     ph_max        = std::acos(std::pow(10, -accuracy_goal / Real(desc.zeta + 1)));
        auto const     ph_min        = -ph_max;
        Range const    a_extent      = { ph_min + M_PI_2, ph_max - ph_min };
        // provisional extent
        Fa_extent.loc = Fa_of_a(a_extent.min());
        Fa_extent.len = Fa_of_a(a_extent.max()) - Fa_extent.loc;
        m_a_of_Fa     = init_integral_table(this, Fa_extent, a_extent, &PartialShellVDF::Fa_of_a);
        // FIXME: Chopping the head and tail off is a hackish solution of fixing anomalous particle initialization close to the boundaries.
        m_a_of_Fa.erase(Fa_extent.min());
        m_a_of_Fa.erase(Fa_extent.max());
        // finalized extent
        Fa_extent.loc = m_a_of_Fa.begin()->first;
        Fa_extent.len = m_a_of_Fa.rbegin()->first - Fa_extent.loc;
    }
}
auto PartialShellVDF::init_integral_table(PartialShellVDF const *self, Range const f_extent, Range const x_extent, Real (PartialShellVDF::*f_of_x)(Real) const noexcept)
    -> std::map<Real, Real>
{
    std::map<Real, Real> table;
    table.insert_or_assign(end(table), f_extent.min(), x_extent.min());
    constexpr long n_samples    = 50000;
    constexpr long n_subsamples = 100;
    auto const     df           = f_extent.len / n_samples;
    auto const     dx           = x_extent.len / (n_samples * n_subsamples);
    Real           x            = x_extent.min();
    Real           f_current    = (self->*f_of_x)(x);
    for (long i = 1; i < n_samples; ++i) {
        Real const f_target = Real(i) * df + f_extent.min();
        while (f_current < f_target)
            f_current = (self->*f_of_x)(x += dx);
        table.insert_or_assign(end(table), f_current, x);
    }
    table.insert_or_assign(end(table), f_extent.max(), x_extent.max());
    return table;
}

auto PartialShellVDF::Fa_of_a(Real const alpha) const noexcept -> Real
{
    static constexpr Real (*const int_cos_zeta)(unsigned, Real) noexcept = [](unsigned const zeta, Real const x) noexcept {
        if (zeta == 0)
            return x;
        if (zeta == 1)
            return std::sin(x);
        return std::pow(std::cos(x), zeta - 1) * std::sin(x) / zeta + Real(zeta - 1) / zeta * int_cos_zeta(zeta - 2, x);
    };
    return int_cos_zeta(desc.zeta + 1, alpha - M_PI_2);
}
auto PartialShellVDF::Fv_of_x(Real const v_by_vth) const noexcept -> Real
{
    auto const xs = m_marker_shell.xs;
    auto const t  = v_by_vth - xs;
    return -(t + 2 * xs) * std::exp(-t * t) + 2 / M_2_SQRTPI * (.5 + xs * xs) * std::erf(t);
}
auto PartialShellVDF::linear_interp(std::map<Real, Real> const &table, Real const x) -> Real
{
    auto const ub = table.upper_bound(x);
    if (ub == end(table) || ub == begin(table))
        throw std::out_of_range{ __PRETTY_FUNCTION__ };
    auto const &[x_min, y_min] = *std::prev(ub);
    auto const &[x_max, y_max] = *ub;
    return (y_min * (x_max - x) + y_max * (x - x_min)) / (x_max - x_min);
}

auto PartialShellVDF::f_common(MFAVector const &v_by_vth, Shell const &shell) noexcept
{
    // note that vel = {v1 - Vd, v2, v3}/vth1
    // f(x1, x2, x3) = exp(-(x - xs)^2)*sin^ζ(α)/(2π θ^3 A(xs) B(ζ))
    //
    auto const x  = std::sqrt(dot(v_by_vth, v_by_vth));
    auto const t  = x - shell.xs;
    Real const fv = std::exp(-t * t) / shell.Ab;
    auto const u  = v_by_vth.x / x; // cos α
    Real const fa = (shell.zeta == 0 ? 1 : std::pow((1 - u) * (1 + u), .5 * shell.zeta)) / shell.Bz;
    return .5 * fv * fa / M_PI;
}
auto PartialShellVDF::f0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_vth, m_shell) / m_vth_cubed;
}
auto PartialShellVDF::g0(CartVector const &vel, CurviCoord const &pos) const noexcept -> Real
{
    auto const shifted_v = geomtr.cart_to_mfa(vel, pos) - m_Vd;
    return Real{ this->n0(pos) } * f_common(shifted_v / m_marker_vth, m_marker_shell) / m_marker_vth_cubed;
}

auto PartialShellVDF::impl_emit(Badge<Super>, unsigned long const n) const -> std::vector<Particle>
{
    std::vector<Particle> ptls(n);
    std::generate(begin(ptls), end(ptls), [this] {
        return this->emit();
    });
    return ptls;
}
auto PartialShellVDF::impl_emit(Badge<Super>) const -> Particle
{
    Particle ptl = load();

    switch (desc.scheme) {
        case ParticleScheme::full_f:
            ptl.psd        = { 1, f0(ptl), g0(ptl) };
            ptl.psd.weight = ptl.psd.real_f / ptl.psd.marker;
            break;
        case ParticleScheme::delta_f:
            ptl.psd = { desc.initial_weight, f0(ptl), g0(ptl) };
            ptl.psd.real_f += ptl.psd.weight * ptl.psd.marker; // f = f_0 + w*g
            break;
    }

    return ptl;
}
auto PartialShellVDF::load() const -> Particle
{
    // position
    //
    CurviCoord const pos{ bit_reversed<2>() * domain_extent.len + domain_extent.loc };

    // velocity in field-aligned, co-moving frame
    //
    Real const ph     = bit_reversed<5>() * 2 * M_PI; // [0, 2pi]
    Real const alpha  = a_of_Fa(bit_reversed<3>() * Fa_extent.len + Fa_extent.loc);
    Real const v_vth  = x_of_Fv(uniform_real<100>() * Fv_extent.len + Fv_extent.loc);
    Real const v1     = std::cos(alpha) * v_vth;
    Real const tmp_v2 = std::sin(alpha) * v_vth;
    Real const v2     = std::cos(ph) * tmp_v2;
    Real const v3     = std::sin(ph) * tmp_v2;

    // velocity in field-aligned frame
    //
    auto const vel = MFAVector{ v1, v2, v3 } * m_marker_vth + m_Vd;

    return { geomtr.mfa_to_cart(vel, pos), pos };
}
LIBPIC_NAMESPACE_END(1)
