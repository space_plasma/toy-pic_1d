/*
 * Copyright (c) 2020-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/VDF.h>

LIBPIC_NAMESPACE_BEGIN(1)
/// Loss-cone velocity distribution function
/// \details
/// f(v1, v2) = exp(-(x1 - xd)^2)/(π^3/2 vth1 vth2^2) * ((1 - Δ*β)*exp(-x2^2) -
/// (1 - / Δ)*exp(-x2^2/β))/(1 - β),
///
/// where x1 = v1/vth1, xd = vd/vth1, x2 = v2/vth2.
/// The effective temperature in the perpendicular direction is 2*T2/vth2^2 = 1 + (1 - Δ)*β
///
class LossconeVDF : public VDF<LossconeVDF> {
    using Super = VDF<LossconeVDF>;

    struct RejectionSampler { // rejection sampler
        RejectionSampler() noexcept = default;
        RejectionSampler(Real Delta, Real beta /*must not be 1*/);
        [[nodiscard]] Real sample() const noexcept;
        // ratio of the target to the proposed distributions
        [[nodiscard]] Real fOg(Real x) const noexcept;
        //
        Real                  Delta;          //!< Δ parameter.
        Real                  beta;           //!< β parameter.
        Real                  alpha;          //!< thermal spread of of the proposed distribution
        Real                  M;              //!< the ratio f(x_pk)/g(x_pk)
        static constexpr Real a_offset = 0.3; //!< optimal value for thermal spread of the proposed distribution
    };

    LossconePlasmaDesc desc;
    RejectionSampler   m_rs;
    //
    Real m_vth1;         //!< Parallel thermal speed.
    Real m_xth2_squared; //!< The ratio of vth2^2 to vth1^2.
    Real m_vth1_cubed;   //!< vth1^3.
    // marker psd parallel thermal speed
    Real m_marker_vth1;
    Real m_marker_vth1_cubed;
    Real m_Nrefcell_div_Ntotal;
    //
    MFAVector m_Vd;
    MFATensor m_vv;

public:
    /// Construct a loss-cone distribution
    /// \note Necessary parameter check is assumed to be done already.
    /// \param desc A BiMaxPlasmaDesc object.
    /// \param geo A geometry object.
    /// \param domain_extent Spatial domain extent.
    /// \param c Light speed. A positive real.
    ///
    LossconeVDF(LossconePlasmaDesc const &desc, Geometry const &geo, Range const &domain_extent, Real c);

    // VDF interfaces
    //
    [[nodiscard]] inline decltype(auto) impl_plasma_desc(Badge<Super>) const noexcept { return (this->desc); }

    [[nodiscard]] inline auto impl_n(Badge<Super>, CurviCoord const &) const { return Scalar{ 1 }; }
    [[nodiscard]] inline auto impl_nV(Badge<Super>, CurviCoord const &pos) const -> CartVector
    {
        return geomtr.mfa_to_cart(m_Vd, pos) * Real{ this->n0(pos) };
    }
    [[nodiscard]] inline auto impl_nvv(Badge<Super>, CurviCoord const &pos) const -> CartTensor
    {
        return geomtr.mfa_to_cart(m_vv, pos) * Real{ this->n0(pos) };
    }

    [[nodiscard]] inline Real impl_Nrefcell_div_Ntotal(Badge<Super>) const { return m_Nrefcell_div_Ntotal; }
    [[nodiscard]] inline Real impl_f(Badge<Super>, Particle const &ptl) const { return f0(ptl); }

    [[nodiscard]] auto impl_emit(Badge<Super>, unsigned long) const -> std::vector<Particle>;
    [[nodiscard]] auto impl_emit(Badge<Super>) const -> Particle;

    // equilibrium physical distribution function
    //
    [[nodiscard]] Real f0(CartVector const &vel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real f0(Particle const &ptl) const noexcept { return f0(ptl.vel, ptl.pos); }

    // marker particle distribution function
    //
    [[nodiscard]] Real g0(CartVector const &vel, CurviCoord const &pos) const noexcept;
    [[nodiscard]] Real g0(Particle const &ptl) const noexcept { return g0(ptl.vel, ptl.pos); }

private:
    [[nodiscard]] Particle load() const;

    // velocity is normalized by vth1 and shifted to drifting plasma frame
    [[nodiscard]] inline static auto f_common(MFAVector const &vel, Real xth2_squared, RejectionSampler const &) noexcept;
};
LIBPIC_NAMESPACE_END(1)
