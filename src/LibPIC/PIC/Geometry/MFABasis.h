/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/CartCoord.h>
#include <PIC/Config.h>
#include <PIC/CurviCoord.h>
#include <PIC/Predefined.h>
#include <PIC/VT/FourTensor.h>
#include <PIC/VT/FourVector.h>
#include <PIC/VT/Vector.h>

#include <cmath>

LIBPIC_NAMESPACE_BEGIN(1)
namespace Detail {
class MFABasis {
    CartVector            m_Bcart_div_B0{};
    ContrVector           m_Bcontr_div_B0{};
    CovarVector           m_Bcovar_div_B0{};
    static constexpr Real m_Bmag_div_B0 = 1;
    //
    CartVector                  m_e1{};
    CartVector                  m_e2{};
    static constexpr CartVector m_e3 = { 0, 0, 1 };

protected:
    explicit MFABasis(Real const theta, CartVector const &D) noexcept
    {
        m_e1 = { std::cos(theta), std::sin(theta), 0 };
        m_e2 = cross(m_e3, m_e1);

        m_Bcart_div_B0  = m_e1;
        m_Bcontr_div_B0 = ContrVector{ m_Bcart_div_B0 / D };
        m_Bcovar_div_B0 = CovarVector{ m_Bcart_div_B0 * D };
    }

public:
    [[nodiscard]] auto theta() const noexcept { return std::atan2(m_e1.y, m_e1.x); }

    /// Magnitude of B/B0
    ///
    [[nodiscard]] auto &Bmag_div_B0(CartCoord const &) const noexcept { return m_Bmag_div_B0; }
    [[nodiscard]] auto &Bmag_div_B0(CurviCoord const &) const noexcept { return m_Bmag_div_B0; }

    /// Contravariant components of B/B0
    ///
    [[nodiscard]] auto &Bcontr_div_B0(CartCoord const &) const noexcept { return m_Bcontr_div_B0; }
    [[nodiscard]] auto &Bcontr_div_B0(CurviCoord const &) const noexcept { return m_Bcontr_div_B0; }

    /// Covariant components of B/B0
    ///
    [[nodiscard]] auto &Bcovar_div_B0(CartCoord const &) const noexcept { return m_Bcovar_div_B0; }
    [[nodiscard]] auto &Bcovar_div_B0(CurviCoord const &) const noexcept { return m_Bcovar_div_B0; }

    /// Cartesian components of B/B0
    ///
    [[nodiscard]] auto &Bcart_div_B0(CartCoord const &) const noexcept { return m_Bcart_div_B0; }
    [[nodiscard]] auto &Bcart_div_B0(CurviCoord const &) const noexcept { return m_Bcart_div_B0; }

    // MARK:- Basis
    [[nodiscard]] auto &e1(CartCoord const &) const noexcept { return m_e1; }
    [[nodiscard]] auto &e2(CartCoord const &) const noexcept { return m_e2; }
    [[nodiscard]] auto &e3(CartCoord const &) const noexcept { return m_e3; }

    [[nodiscard]] auto &e1(CurviCoord const &) const noexcept { return m_e1; }
    [[nodiscard]] auto &e2(CurviCoord const &) const noexcept { return m_e2; }
    [[nodiscard]] auto &e3(CurviCoord const &) const noexcept { return m_e3; }

    // MARK:- Vector Transform
    /// Transformation from field-aligned to cartesian
    ///
    /// \param vmfa A field-aligned vector, {v1, v2, v3}.
    /// \param pos Position.
    /// \return A cartesian vector, {vx, vy, vz}.
    ///
    template <class Coord>
    [[nodiscard]] CartVector mfa_to_cart(MFAVector const &vmfa, Coord const &pos) const noexcept
    {
        return e1(pos) * vmfa.x + e2(pos) * vmfa.y + e3(pos) * vmfa.z;
    }
    /// Transformation from field-aligned to cartesian
    ///
    /// \param vvmfa A diagonal field-aligned tensor, {v1v1, v2v2, v3v3, 0, 0, 0}.
    /// \param pos Position.
    /// \return A symmetric cartesian tensor, {vxvx, vyvy, vzvz, vxvy, vyvz, vzvx}.
    ///
    template <class Coord>
    [[nodiscard]] CartTensor mfa_to_cart(MFATensor const &vvmfa, Coord const &pos) const noexcept
    {
        auto const e1 = this->e1(pos);
        auto const e2 = this->e2(pos);
        auto const e3 = this->e3(pos);
        CartTensor vvcart;
        {
            vvcart.lo() = vvmfa.xx * e1 * e1 + vvmfa.yy * e2 * e2 + vvmfa.zz * e3 * e3;
            vvcart.xy   = e1.x * e1.y * vvmfa.xx + e2.x * e2.y * vvmfa.yy + e3.x * e3.y * vvmfa.zz;
            vvcart.yz   = e1.y * e1.z * vvmfa.xx + e2.y * e2.z * vvmfa.yy + e3.y * e3.z * vvmfa.zz;
            vvcart.zx   = e1.x * e1.z * vvmfa.xx + e2.x * e2.z * vvmfa.yy + e3.x * e3.z * vvmfa.zz;
        }
        return vvcart;
    }

    /// Transformation from cartesian to field-aligned
    ///
    /// \param vcart A cartesian vector, {vx, vy, vz}.
    /// \param pos Position
    /// \return A field-aligned vector, {v1, v2, v3}.
    ///
    template <class Coord>
    [[nodiscard]] MFAVector cart_to_mfa(CartVector const &vcart, Coord const &pos) const noexcept
    {
        return { dot(e1(pos), vcart), dot(e2(pos), vcart), dot(e3(pos), vcart) };
    }
    /// Transformation from cartesian to field-aligned
    ///
    /// \param vvcart A symmetric cartesian tensor, {vxvx, vyvy, vzvz, vxvy, vyvz, vzvx}.
    /// \param pos Position.
    /// \return A diagonal field-aligned tensor, {v1v1, v2v2, v3v3, 0, 0, 0}.
    ///
    template <class Coord>
    [[nodiscard]] MFATensor cart_to_mfa(CartTensor const &vvcart, Coord const &pos) const noexcept
    {
        auto const e1 = this->e1(pos);
        auto const e2 = this->e2(pos);
        auto const e3 = this->e3(pos);
        return {
            dot(vvcart.lo(), e1 * e1) + 2 * (vvcart.xy * e1.x * e1.y + vvcart.zx * e1.x * e1.z + vvcart.yz * e1.y * e1.z),
            dot(vvcart.lo(), e2 * e2) + 2 * (vvcart.xy * e2.x * e2.y + vvcart.zx * e2.x * e2.z + vvcart.yz * e2.y * e2.z),
            dot(vvcart.lo(), e3 * e3) + 2 * (vvcart.xy * e3.x * e3.y + vvcart.zx * e3.x * e3.z + vvcart.yz * e3.y * e3.z),
            0,
            0,
            0,
        };
    }

    /// Transformation from field-aligned to cartesian of the space components of four-vector
    ///
    template <class Coord>
    [[nodiscard]] FourCartVector mfa_to_cart(FourMFAVector const &vmfa, Coord const &pos) const noexcept
    {
        return { vmfa.t, mfa_to_cart(vmfa.s, pos) };
    }
    /// Transformation from field-aligned to cartesian of the space components of four-tensor
    ///
    template <class Coord>
    [[nodiscard]] FourCartTensor mfa_to_cart(FourMFATensor const &vvmfa, Coord const &pos) const noexcept
    {
        return { vvmfa.tt, mfa_to_cart(vvmfa.ts, pos), mfa_to_cart(vvmfa.ss, pos) };
    }

    /// Transformation from cartesian to field-aligned of the space components of four-vector
    ///
    template <class Coord>
    [[nodiscard]] FourMFAVector cart_to_mfa(FourCartVector const &vcart, Coord const &pos) const noexcept
    {
        return { vcart.t, cart_to_mfa(vcart.s, pos) };
    }
    /// Transformation from cartesian to field-aligned of the space components of four-tensor
    ///
    template <class Coord>
    [[nodiscard]] FourMFATensor cart_to_mfa(FourCartTensor const &vvcart, Coord const &pos) const noexcept
    {
        return { vvcart.tt, cart_to_mfa(vvcart.ts, pos), cart_to_mfa(vvcart.ss, pos) };
    }
};
} // namespace Detail
LIBPIC_NAMESPACE_END(1)
