/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "UniformGeometry.h"
#include <limits>

LIBPIC_NAMESPACE_BEGIN(1)
namespace {
constexpr auto quiet_nan  = std::numeric_limits<Real>::quiet_NaN();
constexpr auto quiet_nanV = CartVector{ quiet_nan };
} // namespace
Detail::UniformGeometry::UniformGeometry() noexcept
: CurviBasis{ quiet_nanV }
, MFABasis{ quiet_nan, quiet_nanV }
, m_D{ quiet_nan }
, m_inv_D{ quiet_nan }
, m_sqrt_g{ quiet_nan }
, m_det_gij{ quiet_nan }
{
}
LIBPIC_NAMESPACE_END(1)
