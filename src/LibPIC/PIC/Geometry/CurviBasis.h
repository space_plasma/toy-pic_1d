/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/CartCoord.h>
#include <PIC/Config.h>
#include <PIC/CurviCoord.h>
#include <PIC/VT/Tensor.h>
#include <PIC/VT/Vector.h>

#include <tuple>

LIBPIC_NAMESPACE_BEGIN(1)
namespace Detail {
class CurviBasis {
    std::tuple<CartTensor, CartVector, CartVector, CartVector> m_covar_basis{};
    std::tuple<CartTensor, CartVector, CartVector, CartVector> m_contr_basis{};
    CovarTensor                                                m_covar_metric{};
    ContrTensor                                                m_contr_metric{};

protected:
    explicit constexpr CurviBasis(CartVector const &D) noexcept
    {
        auto const pow2 = [](auto x) noexcept {
            return x * x;
        };

        m_covar_basis
            = std::make_tuple(CartTensor{ D.x, D.y, D.z, 0, 0, 0 },
                              CartVector{ D.x, 0, 0 },
                              CartVector{ 0, D.y, 0 },
                              CartVector{ 0, 0, D.z });
        m_covar_metric = { pow2(D.x), pow2(D.y), pow2(D.z), 0, 0, 0 };

        auto const inv_D = 1 / D;
        m_contr_basis
            = std::make_tuple(CartTensor{ inv_D.x, inv_D.y, inv_D.z, 0, 0, 0 },
                              CartVector{ inv_D.x, 0, 0 },
                              CartVector{ 0, inv_D.y, 0 },
                              CartVector{ 0, 0, inv_D.z });
        m_contr_metric = { pow2(inv_D.x), pow2(inv_D.y), pow2(inv_D.z), 0, 0, 0 };
    }

public:
    /// Calculate covariant components of the metric tensor at a location
    ///
    [[nodiscard]] constexpr auto &covar_metric(CartCoord const &) const noexcept { return m_covar_metric; }
    [[nodiscard]] constexpr auto &covar_metric(CurviCoord const &) const noexcept { return m_covar_metric; }

    /// Calculate contravariant components of the metric tensor at a location
    ///
    [[nodiscard]] constexpr auto &contr_metric(CartCoord const &) const noexcept { return m_contr_metric; }
    [[nodiscard]] constexpr auto &contr_metric(CurviCoord const &) const noexcept { return m_contr_metric; }

    // MARK:- Basis
    /// Calculate i'th covariant basis vectors at a location
    /// \note The index '0' returns all three basis vectors.
    ///
    template <unsigned long i>
    [[nodiscard]] constexpr auto &covar_basis(CartCoord const &) const noexcept
    {
        static_assert(i <= 3, "invalid index range");
        return std::get<i>(m_covar_basis);
    }
    template <unsigned long i>
    [[nodiscard]] constexpr auto &covar_basis(CurviCoord const &) const noexcept
    {
        static_assert(i <= 3, "invalid index range");
        return std::get<i>(m_covar_basis);
    }

    /// Calculate i'th contravariant basis vectors at a location
    /// \note The index '0' returns all three basis vectors.
    ///
    template <unsigned long i>
    [[nodiscard]] constexpr auto &contr_basis(CartCoord const &) const noexcept
    {
        static_assert(i <= 3, "invalid index range");
        return std::get<i>(m_contr_basis);
    }
    template <unsigned long i>
    [[nodiscard]] constexpr auto &contr_basis(CurviCoord const &) const noexcept
    {
        static_assert(i <= 3, "invalid index range");
        return std::get<i>(m_contr_basis);
    }

    // MARK:- Vector Transformation
    /// Vector transformation from contravariant to covariant components
    /// \param contr Contravariant components of a vector.
    /// \param covar_metric Covariant components of the metric tensor at a given location.
    /// \return Covariant components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) contr_to_covar(ContrVector const &contr, CovarTensor const &covar_metric) noexcept { return dot(contr, covar_metric); }

    /// Vector transformation from covariant to contravariant components
    /// \param covar Covariant components of a vector.
    /// \param contr_metric Contravariant components of the metric tensor at a given location.
    /// \return Contravariant components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) covar_to_contr(CovarVector const &covar, ContrTensor const &contr_metric) noexcept { return dot(covar, contr_metric); }

    /// Vector transformation from Cartesian to contravariant components
    /// \param cart Cartesian components of a vector.
    /// \param contr_bases Three contravariant basis vectors at a given location.
    /// \return Contravariant components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) cart_to_contr(CartVector const &cart, CartTensor const &contr_bases) noexcept { return ContrVector{ dot(contr_bases, cart) }; }

    /// Vector transformation from contravariant to Cartesian components
    /// \param contr Contravariant components of a vector.
    /// \param covar_bases Three covariant basis vectors at a given location.
    /// \return Cartesian components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) contr_to_cart(ContrVector const &contr, CartTensor const &covar_bases) noexcept { return dot(CartVector{ contr }, covar_bases); }

    /// Vector transformation from Cartesian to covaraint components
    /// \param cart Cartesian components of a vector.
    /// \param covar_bases Three covariant basis vectors at a given location.
    /// \return Covariant components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) cart_to_covar(CartVector const &cart, CartTensor const &covar_bases) noexcept { return CovarVector{ dot(covar_bases, cart) }; }

    /// Vector transformation from covariant to Cartesian components
    /// \param covar Covariant components of a vector.
    /// \param contr_bases Three contravariant basis vectors at a given location.
    /// \return Cartesian components of the transformed vector.
    [[nodiscard]] static constexpr decltype(auto) covar_to_cart(CovarVector const &covar, CartTensor const &contr_bases) noexcept { return dot(CartVector{ covar }, contr_bases); }

    /// Vector transformation from contravariant to covariant components
    /// \tparam Coord Coordinate type.
    /// \param contr Contravariant components of a vector.
    /// \param pos Current location.
    /// \return Covariant components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) contr_to_covar(ContrVector const &contr, Coord const &pos) const noexcept { return contr_to_covar(contr, covar_metric(pos)); }

    /// Vector transformation from covariant to contravariant components
    /// \tparam Coord Coordinate type.
    /// \param covar Covariant components of a vector.
    /// \param pos Current location.
    /// \return Contravariant components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) covar_to_contr(CovarVector const &covar, Coord const &pos) const noexcept { return covar_to_contr(covar, contr_metric(pos)); }

    /// Vector transformation from Cartesian to contravariant components
    /// \tparam Coord Coordinate type.
    /// \param cart Cartesian components of a vector.
    /// \param pos Current location.
    /// \return Contravariant components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) cart_to_contr(CartVector const &cart, Coord const &pos) const noexcept { return cart_to_contr(cart, contr_basis<0>(pos)); }

    /// Vector transformation from contravariant to Cartesian components
    /// \tparam Coord Coordinate type.
    /// \param contr Contravariant components of a vector.
    /// \param pos Current location.
    /// \return Cartesian components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) contr_to_cart(ContrVector const &contr, Coord const &pos) const noexcept { return contr_to_cart(contr, covar_basis<0>(pos)); }

    /// Vector transformation from Cartesian to covaraint components
    /// \tparam Coord Coordinate type.
    /// \param cart Cartesian components of a vector.
    /// \param pos Current location.
    /// \return Covariant components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) cart_to_covar(CartVector const &cart, Coord const &pos) const noexcept { return cart_to_covar(cart, covar_basis<0>(pos)); }

    /// Vector transformation from covariant to Cartesian components
    /// \tparam Coord Coordinate type.
    /// \param covar Covariant components of a vector.
    /// \param pos Current location.
    /// \return Cartesian components of the transformed vector.
    template <class Coord>
    [[nodiscard]] constexpr decltype(auto) covar_to_cart(CovarVector const &covar, Coord const &pos) const noexcept { return covar_to_cart(covar, contr_basis<0>(pos)); }
};
} // namespace Detail
LIBPIC_NAMESPACE_END(1)
