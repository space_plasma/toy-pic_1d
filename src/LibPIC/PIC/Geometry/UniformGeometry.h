/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <PIC/Config.h>
#include <PIC/Geometry/CurviBasis.h>
#include <PIC/Geometry/MFABasis.h>
#include <PIC/Predefined.h>
#include <PIC/VT/Vector.h>

#include <functional>

LIBPIC_NAMESPACE_BEGIN(1)
namespace Detail {
/// Describes mirror field geometry
///
class UniformGeometry
: public CurviBasis
, public MFABasis {
    Vector m_D;
    Vector m_inv_D;
    Real   m_sqrt_g;
    Real   m_det_gij;

    explicit UniformGeometry(Real const theta, CartVector const &D) noexcept
    : CurviBasis{ D }, MFABasis{ theta, D }
    {
        m_D       = Vector{ D };
        m_inv_D   = 1 / Vector{ D };
        m_sqrt_g  = D.x * D.y * D.z;
        m_det_gij = m_sqrt_g * m_sqrt_g;
    }

public:
    UniformGeometry() noexcept;

    /// Construct a StraightGeometry object
    /// \param D1 Grid scale factor along the first curvilinear coordinate.
    ///
    explicit UniformGeometry(Real const theta, Real const D1)
    : UniformGeometry(theta, { D1, 1, 1 })
    {
    }

    // D
    [[nodiscard]] auto &D() const noexcept { return m_D; }
    [[nodiscard]] auto &D1() const noexcept { return m_D.x; }
    [[nodiscard]] auto &D2() const noexcept { return m_D.y; }
    [[nodiscard]] auto &D3() const noexcept { return m_D.z; }

    // 1/D
    [[nodiscard]] auto &inv_D() const noexcept { return m_inv_D; }
    [[nodiscard]] auto &inv_D1() const noexcept { return m_inv_D.x; }
    [[nodiscard]] auto &inv_D2() const noexcept { return m_inv_D.y; }
    [[nodiscard]] auto &inv_D3() const noexcept { return m_inv_D.z; }

    // √g
    [[nodiscard]] auto &sqrt_g() const noexcept { return m_sqrt_g; }
    // g = det(g_ij)
    [[nodiscard]] auto &det_gij() const noexcept { return m_det_gij; }

    /// From Cartesian to curvilinear coordinate transformation
    /// \param pos Cartesian coordinates.
    /// \return Curvilinear coordinates.
    [[nodiscard]] decltype(auto) cotrans(CartCoord const &pos) const noexcept
    {
        return CurviCoord{ pos.x * m_inv_D.x };
    }

    /// From curvilinear to Cartesian coordinate transformation
    /// \param pos Curvilinear coordinates.
    /// \return Cartesian coordinates.
    [[nodiscard]] decltype(auto) cotrans(CurviCoord const &pos) const noexcept
    {
        return CartCoord{ pos.q1 * m_D.x };
    }
};
} // namespace Detail
LIBPIC_NAMESPACE_END(1)
